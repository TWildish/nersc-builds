#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/7.1
module load python/2.7.4
module load gnustuff/1.0

NPROCS=${NPROCS:=32}

vsn=3.2.1
file=v$vsn.tar.gz
url=https://github.com/libffi/libffi/archive/$file
dir=libffi-$vsn

function build_it() {
  PREFIX=$(cd ..; pwd)/gnu${env}/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  [ -f $file ] || wget -O $file $url
  [ -d $dir ] && rm -rf $dir

  tar xf $file
  cd $dir
  ./autogen.sh
  ./configure --prefix=$PREFIX
# Fool Make into thinking it doesn't need to run 'makeinfo', which we don't have
  touch doc/libffi.info
  make -j $NPROCS
  make install
  cd ..
  rm -rf $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
