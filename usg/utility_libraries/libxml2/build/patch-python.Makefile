--- python/Makefile.orig	2017-10-30 09:10:43.071927645 -0700
+++ python/Makefile	2017-10-30 09:14:52.167460758 -0700
@@ -548,6 +548,9 @@
 	cd $(top_builddir) && $(SHELL) ./config.status $(subdir)/$@
 
 install-pythonLTLIBRARIES: $(python_LTLIBRARIES)
+	echo "Stubbing the python install, do that in python itself instead"
+
+install-pythonLTLIBRARIES-orig: $(python_LTLIBRARIES)
 	@$(NORMAL_INSTALL)
 	@list='$(python_LTLIBRARIES)'; test -n "$(pythondir)" || list=; \
 	list2=; for p in $$list; do \
@@ -643,6 +646,9 @@
 	files=`for p in $$list; do echo $$p; done | sed -e 's|^.*/||'`; \
 	dir='$(DESTDIR)$(docsdir)'; $(am__uninstall_files_from_dir)
 install-dist_pythonDATA: $(dist_python_DATA)
+	echo "Stubbing the python install, do that in python itself instead"
+
+install-dist_pythonDATA-orig: $(dist_python_DATA)
 	@$(NORMAL_INSTALL)
 	@list='$(dist_python_DATA)'; test -n "$(pythondir)" || list=; \
 	if test -n "$$list"; then \
