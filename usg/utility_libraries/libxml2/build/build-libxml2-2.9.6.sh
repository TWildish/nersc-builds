#!/bin/bash

set -ex
cd `dirname $0`

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	module load gnustuff/1.0
	module load zlib/1.2.11
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	[ -d $dir ] && rm -rf $dir
	tar xf $file
	cd $dir
	autoreconf --install
	./configure --prefix=$PREFIX
	patch -p0 < ../patch-python.Makefile
	make -j $NPROCS
	make check
 	make install-strip
 	cd ..
	rm -r $dir
	fix_perms -g usg $PREFIX
}

vsn=2.9.6
file=libxml2-$vsn.tar.xz
url=https://git.gnome.org/browse/libxml2/snapshot/$file
dir=libxml2-$vsn
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

export NPROCS=${NPROCS:=32}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn
done
create-module $PREFIX
