#!/bin/bash

cd `dirname $0`

prerequisites="zlib/1.2.11 libpng/1.6.28 libjpeg/6b libfreetype/2.7.1"
function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load cmake/3.9.0
  module load $prerequisites

  set -ex
  vsn=2.2.4
  file=gd-$vsn.tar.gz
  url=https://github.com/libgd/libgd/archive/$file
  dir=libgd-gd-$vsn
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir
  tar xf $file
  cd $dir

# That's not a typo, it really does look for PNG_PNG_INCLUDE_DIR
# Likewise, FREETYPE_INCLUDE_DIRS, with an 'S' on the end.
# Sheesh...
  cmake \
    -DZLIB_INCLUDE_DIR=$ZLIB_DIR/include -DZLIB_LIBRARY=$ZLIB_DIR/lib/libz.so \
    -DPNG_PNG_INCLUDE_DIR=$LIBPNG_DIR/include -DPNG_LIBRARY=$LIBPNG_DIR/lib/libpng.so \
    -DJPEG_INCLUDE_DIR=$LIBJPEG_DIR/include -DJPEG_LIBRARY=$LIBJPEG_DIR/lib/libjpeg.so \
    -DFREETYPE_INCLUDE_DIRS=$LIBFREETYPE_DIR/include/freetype2 -DFREETYPE_LIBRARY=$LIBFREETYPE_DIR/lib/libfreetype.so \
    -DENABLE_JPEG=1 \
    -DENABLE_PNG=1 \
    -DENABLE_FREETYPE=1 \
    -DCMAKE_INSTALL_PREFIX=$PREFIX \
    -DBUILD_TEST=1 \
    .
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install/strip
  cd ..
  rm -r $dir

  fix_perms -g usg $PREFIX
}
for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX "$prerequisites"
