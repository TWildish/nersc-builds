#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load perl/5.24.0
module load python/2.7-anaconda
module load PrgEnv-gnu/7.1 # 4.8
module load mysql/5.7.19 # 5.6.14
module load cmake/3.9.0 # 3.4.3

export NPROCS=${NPROCS:=32}

vsn_short=5.9
vsn=${vsn_short}.2
dir=qt-everywhere-opensource-src-$vsn
file=$dir.tar.xz
url=http://mirrors.ocf.berkeley.edu/qt/archive/qt/$vsn_short/$vsn/single/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $ir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir

LD_LIBRARY_PATH=`pwd`/qt-everywhere-opensource-src-$vsn/qtbase/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
CXX="g++ -O2"

./configure -prefix $PREFIX \
  -opensource \
  -confirm-license \
  -qt-xcb -static \
  -no-use-gold-linker \
  -skip qtgamepad \
  -skip virtualkeyboard \
  -skip qtserialport
make -j ${NPROCS}
make install
cd ..
rm -rf $dir

fix_perms -g usg $PREFIX
create-module $PREFIX
