#!/bin/bash

set -ex
cd `dirname $0`

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3

	prerequisites="libffi/3.2.1 zlib/1.2.11"
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	module load python/2.7.4
	module load gnustuff/1.0
	module load $prerequisites
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	dir=glib-$version
	[ -d $dir ] && rm -rf $dir
	tar xf $file
	cd $dir
	export LIBFFI_CFLAGS="$LIBFFI_INC"
	export LIBFFI_LIBS="$LIBFFI_LIB -lffi"
	export ZLIB_CFLAGS="$ZLIB_INC"
	export ZLIB_LIBS="$ZLIB_LIB -lz"
	./configure --prefix=$PREFIX --enable-libmount=no --with-pcre=internal
	make -j $NPROCS
 	make install
 	cd ..
	rm -r $dir
	fix_perms -g usg $PREFIX
}

vsn_short=2.54
vsn=${vsn_short}.1
file=glib-$vsn.tar.xz
url=http://ftp.gnome.org/pub/gnome/sources/glib/$vsn_short/$file
dir=glib-$vsn
[ -f $file ] || wget -O $file $url

export NPROCS=${NPROCS:=32}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn
done
create-module $PREFIX "$prerequisites"
