#!/bin/bash

set -ex
cd `dirname $0`

function build_library {
	prgenv=$1
	compiler_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$compiler_version
	export GP_INFINIBAND=0
	module load openmpi/2.1.1
        module load python/2.7-anaconda
	module load bzip2/1.0.6
	module load zlib/1.2.11
	module list
	ORIGDIR=`pwd`
	PREFIX=$(cd ..; pwd)/$prgenv$compiler_version/$version
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX
	fileversion=`echo $version | tr '.' '_'`
	dir=boost_$fileversion
	file=boost_${fileversion}.tar.gz
	url=https://downloads.sourceforge.net/project/boost/boost/$version/$file
	[ -f $file ] || wget -O $file $url
	[ -e $dir ] && rm -fr $dir
	tar xf $file
	cd $dir

	if [ -e $HOME/user-config.jam ]; then
		rm $HOME/user-config.jam
	fi
	echo "using mpi ;" > $HOME/user-config.jam

#	Ugly hack to try to get bzip2 built...
	bzip2vsn=1.0.6
	bzip2file=bzip2-$bzip2vsn.tar.gz
	bzip2dir=bzip2-$bzip2vsn
	bzip2url=http://www.bzip.org/$bzip2vsn/$bzip2file
	[ -f $bzip2file ] || wget -O $bzip2file $bzip2url
	[ -d $bzip2dir ] && rm -rf $bzip2dir
	tar xf $bzip2file
	export BZIP2_SOURCE=`pwd`/$bzip2dir

	export NPROCS=${NPROCS:=32}
	./bootstrap.sh --prefix=$PREFIX --with-python=$PYTHON_DIR
	./b2 -j $NPROCS -a -q variant=release \
		-s ZLIB_INCLUDE=$ZLIB_DIR/include \
		-s ZLIB_LIBPATH=$ZLIB_DIR/lib	\
		-s ZLIB_BINARY=z
#		--with-atomic \
#		--with-chrono \
#		--with-coroutine \
#		--with-context \
#		--with-date_time \
#		--with-exception \
#		--with-filesystem \
#		--with-graph \
#		--with-graph_parallel \
#		--with-iostreams \
#		--with-locale \
#		--with-log \
#		--with-math \
#		--with-mpi \
#		--with-program_options \
#		--with-python \
#		--with-random \
#		--with-regex \
#		--with-serialization \
#		--with-signals \
#		--with-system \
#		--with-test \
#		--with-thread \
#		--with-timer \
#		--with-wave
	./b2 install --prefix=$PREFIX
	cd ..
	rm -r $dir
	cd $ORIGDIR
	fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
 build_library gnu $env 1.63.0
done
create-module $PREFIX
