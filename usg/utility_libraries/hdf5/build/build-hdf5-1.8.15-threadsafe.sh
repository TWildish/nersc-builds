#!/bin/bash

set -ex
cd `dirname $0`

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	opt_version=$4
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
	if [ "x$opt_version" != "x" ]; then
		PREFIX=${PREFIX}_$opt_version
	fi
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	dir=hdf5-${version}
	[ -e $dir ] && rm -rf $dir
	tar xf hdf5-1.8.15-patch1.tar.gz
	cd hdf5-$version
	./configure --prefix=$PREFIX --with-pthread --enable-threadsafe

	export NPROCS=${NPROCS:=32}

	make -j $NPROCS
	make install
	cd ..
	rm -rf $dir
	fix_perms -g usg $PREFIX
}

vsn=1.8.15-patch1
for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn 2>&1 | tee build-hdf5-${vsn}.log
done
create-module $PREFIX
