#!/bin/bash

set -ex
cd `dirname $0`

prerequisites=zlib/1.2.11
function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load $prerequisites

  vsn=1.6.28
  file=libpng-$vsn.tar.gz
  url=ftp://ftp.simplesystems.org/pub/png/src/libpng16/$file
  url2=ftp://ftp.simplesystems.org/pub/png/src/history/libpng16/$file
  dir=libpng-$vsn
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  [ -f $file ] || wget --no-check-certificate -O $file $url || \
                  wget --no-check-certificate -O $file $url2
  [ -d $dir ] && rm -rf $dir
  tar xf $file
  cd $dir

  export CPPFLAGS="$ZLIB_INC"
  export CFLAGS="$ZLIB_INC"
  export LDFLAGS="$ZLIB_LIB"
  ./configure --with-zlib-prefix=$ZLIB_DIR --prefix=$PREFIX
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make check
  make install
  cd ..
  rm -rf $dir

  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX "$prerequisites"
