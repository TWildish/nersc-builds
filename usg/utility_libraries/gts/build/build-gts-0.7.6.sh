#!/bin/bash

set -ex
cd `dirname $0`
NPROCS=${NPROCS:=32}

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	[ -d $dir ] && rm -rf $dir
	tar xf $file
	cd $dir
	./configure --prefix=$PREFIX
	make -j $NPROCS
	make install
	cd ..
	rm -r $dir
	fix_perms -g usg $PREFIX
}

vsn=0.7.6
file=gts-$vsn.tar.gz
url=https://downloads.sourceforge.net/project/gts/gts/$vsn/$file
dir=gts-$vsn
[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn
done
create-module $PREFIX
