#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/6.3
module load gnustuff/1.0

export NPROCS=${NPROCS:=32}

vsn=0.5.3
file=capnproto-c++-$vsn.tar.gz
dir=capnproto-c++-$vsn
url=https://capnproto.org/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
patch -p0 < ../patch.async-io-test
./configure --prefix=$PREFIX
make -j $NPROCS
make check
make install

cd ..
rm -rf $dir
fix_perms -g usg $PREFIX

create-module $PREFIX
