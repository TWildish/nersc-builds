#!/bin/bash
#
# mongo tools (mongorestore etc) are now split out into a separate repository
# (https://github.com/mongodb/mongo-tools)
#

set -ex
cd `dirname $0`
module purge
module load modules
module load PrgEnv-gnu/4.9
module load golang/1.9.2
module load openssl/1.1.0c

#
# Not needed here? How to parallelise 'go build'?
# export NPROCS=${NPROCS:=32}

vsn=3.6.0
file=mongodb-tools-r$vsn.tar.gz
url=https://github.com/mongodb/mongo-tools/archive/r$vsn.tar.gz
dir=mongo-tools-r$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir

mkdir bin
. ./set_gopath.sh
for f in bsondump mongodump mongoexport mongofiles mongoimport mongorestore mongostat mongotop; do
# Don't build mongoreplay because it needs libpcap
  if [ ! -f $PREFIX/bin/$f ] || [ ! -n "$NERSC_BUILD_NO_FORCE_REBUILD" ]; then
    go build -o bin/$f -tags ssl $f/main/$f.go
    strip bin/$f
    mv bin/$f $PREFIX/bin
  fi
done

cd ..
rm -rf $dir
fix_perms -g usg $PREFIX
