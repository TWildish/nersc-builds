#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh
module load libffi/3.2.1
module load libglib/2.54.1

vsn=4.8.19
tool=mc
file=${tool}-${vsn}.tar.bz2
url=http://ftp.midnight-commander.org/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file

cd $dir
./configure --with-screen=ncurses \
  --with-ncurses-includes=$PREFIX/include \
  --with-ncurses-libs=$PREFIX/lib \
  --prefix=$PREFIX \
  GLIB_CFLAGS="$LIBGLIB_INC" \
  GLIB_LIBS="$LIBGLIB_LIB -lglib-2.0"
make -j $NPROCS
make check
make install
cd ..
rm -rf $dir
