#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=1.0.5
tool=makedepend
file=${tool}-${vsn}.tar.gz
url=https://www.x.org/releases/individual/util/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
