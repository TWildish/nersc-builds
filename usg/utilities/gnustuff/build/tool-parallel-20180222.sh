#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

module load gnustuff/1.0

vsn=20180222
tool=parallel
file=${tool}-${vsn}.tar.bz2
url=http://mirrors.ocf.berkeley.edu/gnu/${tool}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
