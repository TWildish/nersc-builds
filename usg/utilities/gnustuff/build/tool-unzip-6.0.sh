#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=60
tool=unzip
file=${tool}${vsn}.tar.gz
url=https://sourceforge.net/projects/infozip/files/UnZip%206.x%20%28latest%29/UnZip%206.0/unzip60.tar.gz
dir=${tool}${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
cp unix/Makefile .
make -j $NPROCS generic CC=gcc
make prefix=$PREFIX install
cd ..
rm -rf $dir
