export VERSION=1.0
export PREFIX=$(cd ..; pwd)/$VERSION

cm=`which create-module`
cm=`dirname $cm`
export PATH=$PREFIX/bin:/usr/bin:/usr/local/bin:/bin:$cm:/usr/common/usg/bin
export LD_LIBRARY_PATH=$PREFIX/lib64:$PREFIX/lib:$LD_LIBRARY_PATH

module purge
module load PrgEnv-gnu/7.1

export NPROCS=${NPROCS:=32}
declare -a epilogue=( m4 autoconf bison flex gperf gettext ncurses automake libtool )
