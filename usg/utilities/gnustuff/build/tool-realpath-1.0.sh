#!/bin/sh

set -ex
cd `dirname $0`

. ./env.sh

[ -f realpath.c ] || wget -O realpath.c https://github.com/dtjm/realpath/raw/master/realpath.c
gcc -o realpath realpath.c
mv realpath $PREFIX/bin/
rm realpath.c
