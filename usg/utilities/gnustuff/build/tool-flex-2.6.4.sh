#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=2.6.4
tool=flex
file=${tool}-${vsn}.tar.gz
url=https://github.com/westes/${tool}/files/981163/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
