--- tmpwatch.c.orig	2017-11-21 09:47:13.462282000 -0800
+++ tmpwatch.c	2017-11-21 09:49:52.677187732 -0800
@@ -665,16 +665,9 @@
 	struct timespec real_clock, boot_clock;
 	time_t boot_time;
 
-	if (clock_gettime(CLOCK_REALTIME, &real_clock) != 0
-	    || clock_gettime(CLOCK_BOOTTIME, &boot_clock) != 0)
-	    message(LOG_FATAL, "Error determining boot time: %s\n",
-		    strerror(errno));
-	boot_time = real_clock.tv_sec - boot_clock.tv_sec;
-	if (real_clock.tv_nsec < boot_clock.tv_nsec)
-	    boot_time--;
-	/* We don't get the values of the two clocks at exactly the same moment,
-	   let's add a few seconds to be extra sure. */
-	boot_time -= 2;
+	if (clock_gettime(CLOCK_REALTIME, &real_clock) != 0 )
+	    message(LOG_FATAL, "Error determining real time: %s\n", strerror(errno));
+	boot_time = real_clock.tv_sec - 5;
 
 	socket_kill_time = boot_time - grace_seconds;
 #else
