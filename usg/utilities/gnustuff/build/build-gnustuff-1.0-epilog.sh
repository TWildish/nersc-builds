#!/bin/bash

# set -ex
cd `dirname $0`
source ./env.sh

PREFIX=$(cd ..; pwd)/1.0
[ ! -f $PREFIX/.need-epilogue ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0

#
# There are dependencies, so build the important ones in order,
# then just build the rest
# This 'ordered' array has to match that in the first build script.
#declare -a ordered=( m4 autoconf bison flex gperf gettext ncurses automake libtool )
declare -a scripts=( `ls tool-*.sh` )

for tool in ${epilogue[@]}
do
  script=`ls tool-${tool}-*.sh`
  for i in "${!scripts[@]}"; do
    if [[ ${scripts[i]} = $script ]]; then
      unset scripts[i]
    fi
  done
done

for tool in ${scripts[@]}
do
  set -ex
  ./$tool
  set +ex
done

[ -f $PREFIX/.need-epilogue ] && rm -f $PREFIX/.need-epilogue
fix_perms -g usg $PREFIX
