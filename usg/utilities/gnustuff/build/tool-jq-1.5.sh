#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

module load gnustuff/1.0

vsn=1.5
tool=jq
file=${tool}-${vsn}.tar.gz
url=https://github.com/stedolan/jq/releases/download/${tool}-${vsn}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
