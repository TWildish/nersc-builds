#!/bin/bash

# set -ex
cd `dirname $0`
source ./env.sh

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
touch $PREFIX/.need-epilogue # Set sentinel for epilogue builds

#
# There are dependencies, so build the important ones in order,
# then just build the rest
#declare -a ordered=( m4 autoconf bison flex gperf gettext ncurses automake libtool )

for tool in ${epilogue[@]}
do
  script=`ls tool-${tool}-*.sh`
  set -ex
  ./$script
  set +ex
done

fix_perms -g usg $PREFIX
create-module $PREFIX
