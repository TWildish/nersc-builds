This package contains lots of tools that don't really deserve a package of their own,
and which don't need to be maintained in multiple versions.

There are dependencies, Midnight Commander (mc) requires libffi, which requires some
of the build tools here. So factor out a prolog and epilog script, to be run at before
and after building libffi, respectively.
