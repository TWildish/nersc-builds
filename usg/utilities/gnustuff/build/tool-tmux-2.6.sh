#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

module load gnustuff/1.0
module load libevent/2.1.8-stable

vsn=2.6
tool=tmux
file=${tool}-${vsn}.tar.gz
url=https://github.com/tmux/tmux/releases/download/${vsn}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
