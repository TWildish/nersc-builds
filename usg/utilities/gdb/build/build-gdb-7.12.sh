#!/bin/bash

set -ex
cd `dirname $0`

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  module purge
  module load PrgEnv-gnu/$env
  module load gnustuff/1.0

  vsn=7.12
  file=gdb-$vsn.tar.gz
  dir=gdb-$vsn
  url=https://ftp.gnu.org/gnu/gdb/$file
  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir

  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
  [ -d $PREFIX ] && rm -rf $PREFIX

  if [ "$env" == "7.1" ]; then
    export CXXFLAGS="-g -O2 -fpermissive" # 7.1 dislikes pointer/integer comparisons
  fi
  tar xf $file
  cd $dir
  ./configure --prefix=$PREFIX

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  # make check # needs DejaGnU
  make install
  cd ..
  rm -rf $dir
  fix_perms -g usg $PREFIX
done
create-module $PREFIX
