#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1

vsn=6.3
tool=texinfo
file=${tool}-${vsn}.tar.gz
url=https://ftp.gnu.org/gnu/${tool}/$file
dir=${tool}-${vsn}
export PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX

export NPROCS=${NPROCS:=32}

make -j $NPROCS
make install
cd ..
rm -rf $dir
fix_perms -g usg $PREFIX
create-module $PREFIX
