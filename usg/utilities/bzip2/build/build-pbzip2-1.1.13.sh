#!/bin/bash

set -ex
cd `dirname $0`

bzip_vsn=1.0.6
vsn=1.1.13
major=`echo $vsn | cut -d\. -f 1`
minor=`echo $vsn | cut -d\. -f 2`
patchlevel=`echo $vsn | cut -d\. -f 3`
short_vsn=${major}.${minor}

file=pbzip2-$vsn.tar.gz
url=https://launchpad.net/pbzip2/$short_vsn/$vsn/+download/$file
dir=pbzip2-$vsn
[ -f $file ] || wget -O $file $url

function build_it() {
  module purge
  module load PrgEnv-gnu/$env

  PREFIX=$(cd ..; pwd)/gnu${env}/$bzip_vsn

  [ ! -f $PREFIX/.need-epilogue ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
# [ -d $PREFIX ] && rm -rf $PREFIX

  [ -d $dir ] && rm -rf $dir

  tar xf $file
  cd $dir
  export LDFLAGS="-I $PREFIX/include -L $PREFIX/lib"
  export NPROCS=${NPROCS:=32}
  make -j $NPROCS LDFLAGS="$LDFLAGS"
  make install PREFIX=$PREFIX
  cd ..
  rm -rf $dir
  fix_perms -g jgitools $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done

rm -f $PREFIX/.need-epilogue
