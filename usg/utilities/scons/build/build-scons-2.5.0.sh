#!/bin/sh

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda

vsn=2.5.0
file=scons-$vsn.tar.gz
url=http://downloads.sourceforge.net/project/scons/scons/$vsn/$file
dir=scons-$vsn

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget $url -O $file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

tar xf $file
cd $dir

python setup.py install --prefix=$PREFIX
cd ..
rm -r $dir
fix_perms -g usg $PREFIX
create-module $PREFIX
