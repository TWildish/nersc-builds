#!/bin/bash

set -ex
cd `dirname $0`

vsn=1.5
file=jq-$vsn.tar.gz
url=https://github.com/stedolan/jq/releases/download/jq-${vsn}/$file
dir=jq-$vsn

module purge
module load PrgEnv-gnu/7.1
module load gnustuff/1.0
[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

tar xf $file
cd $dir

./configure --prefix=$PREFIX --enable-all-static

export NPROCS=${NPROCS:=32}

make -j $NPROCS
# make check # check fails, not sure if I care...
make install
cd ..
rm -r $dir
fix_perms -g usg $PREFIX

create-module $PREFIX
