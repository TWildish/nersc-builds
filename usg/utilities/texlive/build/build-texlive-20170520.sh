#!/bin/bash

#
# Binary distribution, but what version???
#url=http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

set -ex
prerequisites="libglib/2.54.1"
module purge
module load PrgEnv-gnu/7.1
module load gnustuff/1.0
module load $prerequisites

export NPROCS=${NPROCS:=32}

vsn=20170520
file=texlive-${vsn}-source.tar.xz
dir=texlive-${vsn}-source
url=http://mirror.hmc.edu/ctan/systems/texlive/Source/$file
PREFIX=$(cd ..; pwd)/$vsn

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file

cd $dir
TL_MAKE_FLAGS="-j $NPROCS" \
  ./Build --prefix=$PREFIX \
    --disable-web2c \
    --disable-xdvik \
    --with-gd-includes=$LIBGLIB_DIR/include \
    --with-gd-libdir=$LIBGLIB_DIR/lib

cd ..
rm -rf $dir
fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
