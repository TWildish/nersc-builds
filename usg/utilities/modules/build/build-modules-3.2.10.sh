#!/bin/bash

set -ex
cd `dirname $0`

vsn=3.2.10
file=modules-$vsn.tar.gz
dir=modules-$vsn
url=https://downloads.sourceforge.net/project/modules/Modules/$dir/$file
PREFIX=`pwd`/install
NPROCS=${NPROCS:=16}

tclvsn=8.6.7
tclfile=tcl${tclvsn}-src.tar.gz
tclurl=https://prdownloads.sourceforge.net/tcl/$tclfile
tcldir=tcl${tclvsn}
[ -f $tclfile ] || wget -O $tclfile $tclurl
[ -d $tcldir ] && rm -rf $tcldir
tar xf $tclfile
cd $tcldir/unix
./configure
make -j $NPROCS
make install
cd ../..
rm -rf $tcldir

[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
export CPPFLAGS=-DUSE_INTERP_ERRORLINE
export LDFLAGS=-static
./configure --with-static=yes --prefix=$PREFIX
make -j 4
make install
cd ..
rm -rf $dir
 
echo "$PREFIX/Modules/3.2.10/bin/modulecmd sh \$@" | tee $PREFIX/module
chmod +x $PREFIX/module
create-module $PREFIX
