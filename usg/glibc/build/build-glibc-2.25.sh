#!/bin/bash

set -e
module purge

echo "Need to figure out what to do with the kernel headers..."
exit 0
cd `dirname $0`

NPROCS=${NPROCS:=32}
vsn=2.25
file=glibc-$vsn.tar.bz2
dir=glibc-$vsn
objdir=obj-$dir
url=http://mirrors.ocf.berkeley.edu/gnu/glibc/$file

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
[ -d $objdir ] && rm -rf $objdir
mkdir $objdir
cd $objdir
export CPPFLAGS="-I /usr/include -I /usr/include/linux"
export CFLAGS="-I /usr/include -I /usr/include/linux"
../$dir/configure --prefix=/usr
make -j $NPROCS
make install
cd ..
rm -rf $dir $objdir
