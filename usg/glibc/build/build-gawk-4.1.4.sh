#!/bin/bash

set -e
module purge

echo "Need to figure out what to do with the kernel headers..."
exit 0
cd `dirname $0`

NPROCS=${NPROCS:=32}
vsn=4.1.4
file=gawk-$vsn.tar.xz
dir=gawk-$vsn
objdir=obj-$dir
url=http://mirrors.ocf.berkeley.edu/gnu/gawk/$file

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
[ -d $objdir ] && rm -rf $objdir
mkdir $objdir
cd $objdir
../$dir/configure 
make -j $NPROCS
make install
cd ..
rm -rf $dir $objdir
