#!/bin/bash
set -ex
cd `dirname $0`

VER=2.3
TARBALL=gsl-$VER.tar.gz
URL=http://gnu.mirror.constant.com/gsl/$TARBALL
BASE_PREFIX=$(cd ..; pwd)

if [ ! -e $TARBALL ]; then
	wget -O $TARBALL "$URL"
fi

export NPROCS=${NPROCS:=32}

function build_library {
	compiler=$1
	compiler_version=$2
	version=$3
	CC=$4
	VERSION=$version
	echo " "
	echo "Start building $VERSION for $CC $compiler_version"

	module purge
	module load PrgEnv-$compiler/$compiler_version
	module list

	PREFIX=$BASE_PREFIX/$compiler$compiler_version/${version}
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX
	
	mkdir -p $PREFIX/{bin,lib,include/bam,share/man/man1}

	[ -d gsl-$version ] && rm -rf gsl-$version
	tar xf $TARBALL
	cd gsl-$version
	
	./configure --prefix=$PREFIX

	make -j $NPROCS
	make check
	make install

	cd ..
	rm -fr gsl-$version

	fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
		egrep ^PrgEnv | grep -v usr | \
		cut -f2 -d/ | \
		cut -f1 -d\ `
do
	build_library gnu $env $VER gcc
done
create-module $PREFIX
