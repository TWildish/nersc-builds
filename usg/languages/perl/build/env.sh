set -ex
# export PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/common/usg/bin
module purge
module load gnustuff/1.0
module load PrgEnv-gnu/7.1
module load oracle_client/11.2.0.3.0

echo Using `which gcc`
gcc -v

if [ "$NERSC_BUILD_ALLOW_ANY_USER" != "true" ]; then
  if [ "$USER" != "gpinterp" ]; then
    echo "Please install this s/w via the gpinterp account"
    exit 1 # 'exit' isn't ideal, I'm being sourced...
  fi
fi

if [ "$vsn"  == "" ]; then
  echo "Need to define vsn in your environment"
  exit 1 # 'exit' isn't ideal, I'm being sourced...
fi

export PREFIX=$(cd ..; pwd)/$vsn

# Sanitize environment!
for e in `env | egrep ^PERL | awk -F= '{ print $1 }'`; do
  echo unset $e
  unset $e
done

export PERL_MM_USE_DEFAULT=1 # Don't prompt for input, take defaults all the way
export PERL_LOCAL_LIB_ROOT=$PREFIX/extra
export PERL5LIB=$PERL_LOCAL_LIB_ROOT/lib/perl5/x86_64-linux-thread-multi:$PERL_LOCAL_LIB_ROOT/lib/perl5:$PERL_LOCAL_LIB_ROOT/lib/site_perl/5.24.0/x86_64-linux-thread-multi

export PATH=$PREFIX/bin:$PATH
/usr/bin/which perl

[ -d $HOME/.cpanplus ] && rm -rf $HOME/.cpanplus
[ -d $HOME/.cpanm    ] && rm -rf $HOME/.cpanm
export PERL_CPANM_HOME=$HOME/.cpanm-$vsn-`hostname`
[ -d $PERL_CPANM_HOME ] && rm -rf $PERL_CPANM_HOME

# the [ -d's ] above return 1 if there's no directory, which will kill us if we
# run with 'set -e'. Make sure we exit with status zero if we get this far.
# However, don't "exit 0", because I don't want to exit, I'm being sourced!
/bin/true
