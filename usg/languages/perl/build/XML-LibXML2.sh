#!/bin/sh

set -e
. ./env.sh

#
# Need to get a bunch of our libraries into the path, or things don't work properly.
# N.B. Be careful of interaction between modules here and modules as defined in env.sh!
#
# module load PrgEnv-gnu/7.1 libxml2/2.9.6

set -ex

#
# start with the dependencies
./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT XML::LibXML

XML_vsn=2.0132
file=XML-LibXML-$XML_vsn.tar.gz
dir=XML-LibXML-$XML_vsn
url=http://search.cpan.org/CPAN/authors/id/S/SH/SHLOMIF/$file
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file
cd $dir

#
# The XML build process looks for libxml2, which we don't have properly.
#perl ./Makefile.PL LIBS="$LIBXML2_LIB"
perl ./Makefile.PL LIBS="-L/usr/lib64"
make -j $NPROCS
# make test fails, but who cares...
# make test
make install

cd ..
rm -rf $dir

# Record the version installed
./cpanm --info XML::LibXML | egrep -v '^!' | tee -a $INSTALLED
