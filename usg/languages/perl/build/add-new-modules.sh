#!/bin/bash

module_list=$1
if [ -z "$module_list" ]; then
  module_list=new-modules.txt
fi
if [ ! -f $module_list ]; then
  echo "No module list given or found: $module_list doesn't exist"
  exit 0
fi

echo Using modules in $module_list

cd `dirname $0`
for vsn in `ls .. | egrep "[[:digit:]](\.[[:digit:]]*)+"`
do
  . ./env.sh
  set +ex
  rm -rf ~/.cpanm-$vsn-`hostname`
  for module in `cat $module_list | egrep -v '^#'`
  do
    echo Install $module in $vsn
    ./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT $module
    ./cpanm -l $PERL_LOCAL_LIB_ROOT $module
  done

  cat $module_list >> modules.$vsn.list
  ./cpanm --info $module | egrep -v '^!' | tee -a modules.installed.$vsn.list
  cpan -l | tee MANIFEST.$vsn

  REALUSER=`id --user --name`
  fix_perms -g $REALUSER ../$vsn/extra
  fix_perms -g $REALUSER .
done
chmod o+r *
