#!/bin/sh

set -e
. ./env.sh

#
# Need to get a bunch of our libraries into the path, or things don't work properly.
# N.B. Be careful of interaction between modules here and modules as defined in env.sh!
#
module load libgd/2.2.4 libpng/1.6.28 zlib/1.2.11 libjpeg/6b libfreetype/2.7.1
which gcc
gcc -v

set -e
which perl

GD_vsn=2.56
file=GD-$GD_vsn.tar.gz
dir=GD-$GD_vsn
url=http://search.cpan.org/CPAN/authors/id/L/LD/LDS/$file
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar xvf $file
cd $dir

#
# The GD build process looks for gdlib-config, to specify the libgd build
# parameters. Newer libgd versions don't provide this anymore, so fake it!
(
  echo "echo GD library  2.2.4"
  echo "echo includedir: $LIBGD_DIR/include"
  echo "echo cflags:     $LIBGD_INC"
  echo "echo ldflags:    $LIBGD_LIB $LIBPNG_LIB $ZLIB_LIB $LIBJPEG_LIB $LIBFREETYPE_LIB -L/usr/lib64 -L. -Wl,-z,relro"
  echo "echo libs:       -lXpm -lX11 -ljpeg -lfontconfig -lfreetype -lpng -lz -lm"  
  echo "echo libdir:     $LIBGD_DIR/lib64"
  echo "echo features:   GD_XPM GD_JPEG GD_FONTCONFIG GD_FREETYPE GD_PNG GD_GIF GD_GIFANIM GD_OPENPOLYGON"
) | tee gdlib-config >/dev/null
#
# Also, we don't provide the base version of libfontconfig.so or libXpm.so, so fake them here too
ln -s /usr/lib64/libfontconfig.so.1 libfontconfig.so
ln -s /usr/lib64/libXpm.so.4 libXpm.so
chmod +x gdlib-config
export PATH=${PATH}:`pwd`

perl ./Build.PL
./Build

#
# test#7 fails, because of libfreetype, but it's not significant
# see https://rt.cpan.org/Public/Bug/Display.html?id=106594
# Fortunately, #7 is the last test, so just trim the list of tests to ignore it!
chmod 644 t/GD.t
patch -p0 < ../patch-GD
./Build test # (optional)
./Build install
cd ..
rm -rf $dir

module="GD::Graph::histogram"
echo " "
echo "Install dependencies for $module"
./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT $module
echo "..."
echo "Install $module"
./cpanm -l $PERL_LOCAL_LIB_ROOT $module
echo " "

# Record the version installed
./cpanm --info GD | egrep -v '^!' | tee -a $INSTALLED
./cpanm --info $module | egrep -v '^!' | tee -a $INSTALLED
