--- sv.c.bak	2017-03-07 14:23:49.452349345 -0800
+++ sv.c	2017-03-07 14:24:10.846800926 -0800
@@ -2008,7 +2008,7 @@
 	    if (SvNVX(sv) == (NV) SvIVX(sv)
 #ifndef NV_PRESERVES_UV
 		&& (((UV)1 << NV_PRESERVES_UV_BITS) >
-		    (UV)(SvIVX(sv) > 0 ? SvIVX(sv) : -SvIVX(sv)))
+		    (UV)(SvIVX(sv) > 0 ? SvUVX(sv) : -SvIVX(sv)))
 		/* Don't flag it as "accurately an integer" if the number
 		   came from a (by definition imprecise) NV operation, and
 		   we're outside the range of NV integer precision */
