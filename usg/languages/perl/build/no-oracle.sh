#!/bin/bash

set -ex
for f in modules*list
do
  echo $f
  [ -f $f.bak ] && rm -f $f.bak
  mv $f $f.bak
  cat $f.bak | grep -v DBD::Oracle | tee $f >/dev/null
done
