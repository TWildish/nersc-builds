#!/bin/bash

cd `dirname $0`

vsn=$1
if [ -z $vsn ]; then
  echo "Need the version number as the only argument"
  exit 1
fi

set -ex
PREFIX=$(cd ..; pwd)/${vsn}-anaconda

PATH=${PREFIX}/bin:$PATH
LD_LIBRARY_PATH=${PREFIX}/bin:$LD_LIBRARY_PATH

pip --no-cache install --requirement packages.txt
pip freeze | tee MANIFEST.$vsn.txt
