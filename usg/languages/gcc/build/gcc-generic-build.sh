#!/bin/bash

for var in gcc_vsn binutils_vsn
do
  eval value=\$$var
  echo "$var = $value"
  if [ "$value" == "" ]; then
    echo "Required variable '$var' not set, aborting"
    exit 0
  fi
done

set -ex
module purge
if [ "$build_with" != "" ]; then
  module load gcc/$build_with
fi

export NPROCS=${NPROCS:=32}

PREFIX=$(cd ..; pwd)/${gcc_vsn}${gcc_variant}
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

export PATH=${PREFIX}/bin:$PATH
export LD_LIBRARY_PATH=${PREFIX}/lib:$LD_LIBRARY_PATH

binutils_dir=binutils-$binutils_vsn
binutils_file=binutils-$binutils_vsn.tar.gz
binutils_url=https://ftp.gnu.org/gnu/binutils/$binutils_file
[ -e $binutils_dir ] && rm -rf $binutils_dir
[ -f $binutils_file ] || wget --no-check-certificate -O $binutils_file $binutils_url
tar xf $binutils_file

objdir=obj-$binutils_dir
[ -d $objdir ] && rm -rf $objdir
mkdir -p $objdir
cd $objdir
../$binutils_dir/configure --prefix=$PREFIX LDFLAGS="--static"
make -j $NPROCS
make install-strip
cd ..
rm -rf $binutils_dir $objdir

file=gcc-$gcc_vsn.tar.gz
file_repacked=gcc-$gcc_vsn.repacked.tar.gz
url=http://mirrors.ocf.berkeley.edu/gnu/gcc/gcc-$gcc_vsn/$file
dir=gcc-$gcc_vsn
[ -d $dir ] && echo "Cleaning up first" && rm -rf $dir

if [ -f $file_repacked ]; then
  tar xf $file_repacked
else
  [ -f $file ] || wget -O $file $url
  echo Unpacking sources
  mkdir $dir
  tar xf $file -C $dir --strip-components=1

  echo "Downloading prerequisites"
  cd $dir
  ./contrib/download_prerequisites
  cd ..
  tar zcf $file_repacked $dir
  rm -f $file
fi

objdir=obj-$dir
[ -d $objdir ] && rm -rf $objdir
mkdir $objdir
cd $objdir

../$dir/configure \
  --prefix=$PREFIX \
  --disable-multilib \
  --enable-languages=c,c++,fortran,objc,obj-c++${extra_languages}
make -j $NPROCS
make install-strip
cd ..
rm -rf $dir $objdir
fix_perms -g usg $PREFIX
create-module $PREFIX
create-PrgEnv-module $gcc_vsn
