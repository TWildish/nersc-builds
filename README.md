# README #

This is where I'm tracking my build scripts for NERSC s/w

Best practices, to enforce:
- Use -fgcc-record-switches when using gcc 5.x or higher
  - use 'readelf -p .GCC.command.line $exe' to see what was used to build $exe
- Use make check
- Use "cd $(dirname $0)"
- Set PREFIX=$(cd ..; pwd)
- Use module purge, load all modules with explicit versions

- Clear the MODULEPATH, set it explicitly
- set PATH and LD_LIBRARY_PATH explicitly
- clear Perl and Python environment variables

Still do to:

How to run it:
- cd ./bin
- ./check-build-order.sh # make sure that what's left out should be left out
- ./make-build-everything.sh | tee doit.sh
- chmod +x doit.sh

then run the doit.sh script. It will take a loooong time, about 15-20 hours, if
you're lucky. Often it will stop because of a silly glitch on the filesystem or
something like that. Then just restart it, it won't rebuild most of the stuff if
it sees it's already there.

If you're running on a login node, and want to be kind to people, set the NPROCS
environment variable to the number of processors to use for parallel compilation.
Otherwise it'll take 32 cores when it can.
