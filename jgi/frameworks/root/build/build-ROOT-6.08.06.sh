#!/bin/bash

set -ex

prerequisites="PrgEnv-gnu/7.1 python/2.7-anaconda pcre/8.40 openssl/1.1.0c R/3.4.1R2 gsl/2.3"
module purge
module load $prerequisites
module load mysql/5.7.19
module load cmake/3.9.0

export NPROCS=${NPROCS:=32}

vsn=6.08.06
file=root_v${vsn}.source.tar.gz
url=https://root.cern.ch/download/$file
dir=root-${vsn}
builddir=ROOT-build-$vsn
PREFIX=$(cd ..; pwd)/$vsn
ROOTSYS=$PREFIX

[ -f $file ] || wget -O $file $url
[ -d $PREFIX ] && rm -rf $PREFIX
[ -d $dir ] && rm -rf $dir
tar xf $file

[ -d $builddir ] && rm -rf $builddir
mkdir -p $builddir
cd $builddir
cmake -G"Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$PREFIX -Dgnuinstall=ON ../$dir
make -j ${NPROCS}
make install
cd ..

rm -rf $dir $builddir
fix_perms -g usg $PREFIX/..
create-module $PREFIX $prerequisites
