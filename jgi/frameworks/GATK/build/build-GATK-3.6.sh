#!/bin/bash

# Go to https://software.broadinstitute.org/gatk/download/
# and log in to download the tarball

set -ex
vsn=3.6
file=GenomeAnalysisTK-$vsn.tar.bz2
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -d $PREFIX ] || mkdir -p $PREFIX

if [ ! -f $file ]; then
  echo "You don't have the source file, $file"
  echo "Go to https://software.broadinstitute.org/gatk and download it!"
  exit 1
fi

tar --directory=$PREFIX -xf $file

fix_perms -g usg $PREFIX/
create-module $PREFIX
