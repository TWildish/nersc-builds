#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1

vsn=6.6.0
dir=EMBOSS-$vsn
file=EMBOSS-${vsn}.tar.gz
url=ftp://emboss.open-bio.org/pub/EMBOSS/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir
CFLAGS=$GCC_RECORD_SWITCHES ./configure --prefix=$PREFIX --without-x

export NPROCS=${NPROCS:=32}

make -j $NPROCS
make install
cd ..
rm -rf $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
