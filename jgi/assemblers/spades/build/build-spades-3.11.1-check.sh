#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load cmake/3.9.0
module load zlib/1.2.11
module load bzip2/1.0.6
module load readline/7.0
module load gnustuff/1.0

export NPROCS=${NPROCS:=32}

vsn=3.11.1-check
export PREFIX=$(cd ..; pwd)/$vsn
file=SPAdes-$vsn.tar.gz
url=http://spades.bioinf.spbau.ru/release$vsn/$file
url=http://cab.spbu.ru/files/$file
dir=SPAdes-$vsn
[ -f $file ] || wget -q -O $file $url
[ -d $dir ] && rm -rf $dir

echo Using PREFIX=$PREFIX
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

tar xf $file
cd $dir

#
# Don't use spades_compile.sh because it doesn't handle the complex arguments we need to
# pass it. Do it all by hand here instead.
#

BUILD_DIR=build_spades
BASEDIR=`pwd`
mkdir -p $BUILD_DIR
cd $BUILD_DIR

#
# For the record, I hate cmake with a deep, passionate, primal loathing
# _all_ of the -I/-L options below are needed, which is absurd!
#
FLAGS="-frecord-gcc-switches $BZIP2_INC $ZLIB_LIB"
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$PREFIX" \
  -DCMAKE_CXX_FLAGS="$FLAGS" \
  -DCMAKE_C_FLAGS="$FLAGS" \
  \
  -DZLIB_LIBRARY=$ZLIB_DIR/lib/libz.so \
  -DZLIB_INCLUDE_DIR=$ZLIB_DIR/include \
  \
  -DBZIP2_LIBRARIES=$BZIP2_DIR/lib/libbz2.so \
  -DBZIP2_INCLUDE_DIR=$BZIP2_DIR/include \
  \
  -DSPADES_STATIC_BUILD:BOOL=ON \
  ../src

make -j $NPROCS
make install/strip
cd ../..
rm -r $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
