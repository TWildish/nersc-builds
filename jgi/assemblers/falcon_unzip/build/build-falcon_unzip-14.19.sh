#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

prerequisites="samtools/1.4 falcon/1.8.8 smrtlink/5.0.0.6792"
module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda
#module load git/2.11.0
#module load openssl/1.1.0c
module load $prerequisites

vsn=14.19
PREFIX=$(cd ..; pwd)/$vsn
file=falcon-2018.02.16-${vsn}-py2.7-ucs4.tar.gz
url=https://downloads.pacbcloud.com/public/falcon/$file
here=`pwd`

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

mkdir -p $PREFIX
cd $PREFIX

tar --strip-components=1 -xf ${here}/${file}
#python setup.py install

fix_perms -g jgitools $PREFIX
create-module $PREFIX "$prerequisites"
echo create-module $PREFIX "$prerequisites"
