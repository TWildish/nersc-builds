#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

prerequisites="samtools/1.4 falcon/1.8.8 smrtlink/5.0.0.6792"
module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda
module load git/2.11.0
module load openssl/1.1.0c
module load $prerequisites

vsn=0.4.0
PREFIX=$(cd ..; pwd)/$vsn
file=`pwd`/FALCON_unzip.$vsn.tgz

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -d $vsn ] && rm -rf $vsn

if [ ! -f $file ]; then
  git clone git://github.com/PacificBiosciences/FALCON_unzip $vsn
  cd $vsn
  git checkout $vsn
# git submodule update --init --recursive
  cd ..
  tar zcf $file $vsn
  rm -rf $vsn
fi

mkdir -p $PREFIX
cd $PREFIX

tar --strip-components=1 -xf $file
python setup.py install

fix_perms -g jgitools $PREFIX
create-module $PREFIX "$prerequisites"
echo create-module $PREFIX "$prerequisites"
