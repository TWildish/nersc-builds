#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/7.1
module load cmake/3.9.0
module load boost/1.63.0
module load gnuplot/5.0.5

vsn=2.2.5
file=Meraculous-v$vsn.tar.bz2
#
# Open-source version
# url=https://downloads.sourceforge.net/project/meraculous20/$file
#
# From the repository
url=https://bitbucket.org/berkeleylab/genomics-meraculous2/get/v${vsn}.tar.bz2
dir=Meraculous-v$vsn

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

mkdir -p $dir
tar xf $file --directory=$dir --strip-components=1
cd $dir
export BOOST_ROOT=$BOOST_DIR
./install.sh $PREFIX
cd ..
rm -rf $dir
fix_perms -g jgi $PREFIX
