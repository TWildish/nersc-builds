#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/4.9 # doesn't work with gcc 5 or above :-(
module load cmake/3.9.0
module load boost/1.59.0
module load gnuplot/5.0.5

vsn=2.2.2.5_lt
file=release-$vsn.tgz
url=https://downloads.sourceforge.net/project/meraculous20/$file
dir=release-$vsn

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

set -e
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir
./install.sh $PREFIX
cd ..
rm -rf $dir
fix_perms -g jgi $PREFIX
create-module $PREFIX
