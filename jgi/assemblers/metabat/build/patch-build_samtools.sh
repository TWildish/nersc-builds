--- build_samtools.sh.orig	2017-11-29 10:14:51.539086460 -0800
+++ build_samtools.sh	2017-11-29 10:16:17.004572358 -0800
@@ -25,11 +25,11 @@
 
 cd htslib-$htslibver
 ./configure --prefix=$inst
-make && make install
+make -j $NPROCS && make install
 
 cd ..
 
-make
+make LIBCURSES="-L$GNUSTUFF_DIR/lib -lncurses" -j $NPROCS
 make prefix=$inst install
 cp libbam.a $inst/lib
 cp *.h $inst/include/bam
