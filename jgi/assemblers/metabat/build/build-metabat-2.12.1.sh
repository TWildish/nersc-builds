#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

module purge
module load PrgEnv-gnu/7.1
module load zlib/1.2.11
module load boost/1.63.0
module load scons/2.5.0
module load gnustuff/1.0

vsn=2.12.1
file=v$vsn.tar.bz2
dir=metabat-$vsn
url=https://bitbucket.org/berkeleylab/metabat/get/v2.12.1.tar.bz2

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
mkdir -p $dir

tar --directory $dir --strip-components 1 -xf $file
cd $dir
patch -p0 < ../patch-build_samtools.sh
scons -j ${NPROCS} install PREFIX=$PREFIX BOOST_ROOT=$BOOST_ROOT

cd ..
rm -rf $dir
