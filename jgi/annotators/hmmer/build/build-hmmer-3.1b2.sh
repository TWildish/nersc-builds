#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1

export NPROCS=${NPROCS:=32}

vsn=3.1b2
file=hmmer-$vsn.tar.gz
url=http://eddylab.org/software/hmmer3/$vsn/$file
dir=hmmer-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir

[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
LDFLAGS="-static" ./configure --prefix=$PREFIX
make -j $NPROCS
make check
#
# Bill's version causes two tests to fail, #252 and #253.
# Bill claims these aren't relevant to JGI, so go ahead
cp ../hpc_hmmsearch src/hmmsearch
make install

cd ..
rm -rf $dir
fix_perms -g jgi $PREFIX
create-module $PREFIX
