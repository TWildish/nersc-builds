#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/7.1

export NPROCS=${NPROCS:=32}

vsn=2.6.3
file=v$vsn.tar.gz
dir=Prodigal-$vsn
url=https://github.com/hyattpd/Prodigal/archive/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir
make INSTALLDIR=$PREFIX/bin -j $NPROCS install
cd ..
rm -rf $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
