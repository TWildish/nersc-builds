#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/7.1
module load gnustuff/1.0

export NPROCS=${NPROCS:=32}

vsn=912
file=last-$vsn.zip
url=http://last.cbrc.jp/$file
dir=last-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

unzip $file
cd $dir
make -j $NPROCS CXXFLAGS="-O3 -mtune=native -std=c++11 -pthread -DHAS_CXX_THREADS -static-libstdc++"
mkdir -p $PREFIX
make install prefix=$PREFIX
cd ..
rm -rf last-$vsn

fix_perms -g jgitools $PREFIX
create-module $PREFIX
