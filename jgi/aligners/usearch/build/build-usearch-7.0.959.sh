#!/bin/bash

set -ex
vsn=7.0.959
file=usearch.${vsn}.tar.xz
PREFIX=$(cd ..; pwd)/$vsn

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

if [ ! -f $file ]; then
  echo "Cannot download usearch, it's licensed software"
  echo "Check https://www.drive5.com/ for details"
  exit 1
fi

mkdir -p $PREFIX
tar --strip-components=1 --directory=$PREFIX -xf $file
fix_perms -g jgi $PREFIX
create-module $PREFIX
