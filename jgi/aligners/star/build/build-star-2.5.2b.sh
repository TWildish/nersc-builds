#!/bin/bash

set -ex
module purge
module load PrgEnv-gnu/7.1

export NPROCS=${NPROCS:=32}

vsn=2.5.2b
dir=STAR
url=https://github.com/alexdobin/STAR.git
PREFIX=$(cd ..; pwd)/$vsn

[ -d $PREFIX ] && rm -rf $PREFIX
[ -d $dir ] && rm -rf $dir

git clone https://github.com/alexdobin/STAR.git
cd $dir
git checkout $vsn

cd source
make -j $NPROCS STAR
cd ..

mkdir $PREFIX
cp bin/Linux_x86_64_static/* $PREFIX
cd ..
rm -rf $dir

fix_perms -g jgitools $PREFIX
