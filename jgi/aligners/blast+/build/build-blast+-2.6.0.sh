#!/bin/bash

set -ex
cd `dirname $0`

prerequisites="libpng/1.6.28 pcre/8.40 libjpeg/6b zlib/1.2.11 bzip2/1.0.6 boost/1.63.0 curl/7.52.1 openssl/1.1.0c"
module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda
module load perl/5.24.0
module load mysql/5.7.19
module load "$prerequisites"
unset PCRE_LIB # That variable is used by the configure scripts :/

#
# Would like to use our own hdf5 and mongodb, but can't be bothered to debug
# why it doesn't like them.
#module load hdf5/1.8.15-patch1
#module load mongodb/3.4.6

vsn=2.6.0
file=ncbi-blast-${vsn}+-src.tar.gz
url=ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/$vsn/$file
dir=ncbi-blast-${vsn}+-src
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

[ -d $dir ] && rm -rf $dir
tar xf $file

cd $dir/c++
# used to have "-static" in the LDFLAGS, but that doesn't work now. Something
# about the curl/openssl libraries it doesn't like.
./configure --prefix=$PREFIX \
	--with-bin-release \
	--with-mt \
	--with-64 \
	--without-debug \
	--with-optimization \
	LDFLAGS="$CURL_LIB -l curl $OPENSSL_LIB -lssl -lcrypto $ZLIB_LIB -lz" \
	--with-static \
	--with-openmp \
	--with-z=$ZLIB_DIR \
	--with-png=$LIBPNG_DIR \
	--with-jpeg=$LIBJPEG_DIR \
	--with-pcre=$PCRE_DIR \
	--with-python=$PYTHON_DIR \
	--with-perl=$PERL_DIR \
	--with-boost=$BOOST_DIR \
	--with-curl=$CURL_DIR \
	--with-mysql=$MYSQL_DIR \
	--with-openssl=$OPENSSL_DIR \
	--with-bz2=$BZIP2_DIR

export NPROCS=${NPROCS:=32}

make -j $NPROCS \
  CFLAGS="$GCC_RECORD_SWITCHES" \
  CXXFLAGS="$GCC_RECORD_SWITCHES"

# make check doesnt do anything useful, unfortunately :-(
# make check

# make install has an error because include/common/ already exists, having
# been created at some point earlier in the install.
# just remove the duplicate first, it has nothing useful anyway.
rm -rf ReleaseMT/inc/common
make install

cd ../..
rm -r $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX "$prerequisites"
