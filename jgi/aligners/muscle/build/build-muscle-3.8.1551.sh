#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

module purge
module load PrgEnv-gnu/7.1

vsn=3.8.1551
file=muscle${vsn}_src.tar.gz
dir=muscle${vsn}
url=http://www.drive5.com/muscle/muscle_src_${vsn}.tar.gz
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

mkdir -p $dir
cd $dir
tar xf ../$file
make -j $NPROCS
mkdir -p $PREFIX/bin
cp muscle $PREFIX/bin
cd ..
rm -rf $dir

fix_perms -g jgitools $PREFIX
create-module $PREFIX
