#!/bin/bash

cd `dirname $0`
set -ex

vsn=3.9.1-0
file=clang-upc-all-${vsn}.tar.gz
dir=clang-upc-${vsn}
url=https://github.com/Intrepid/clang-upc/releases/download/${dir}/$file
[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file

function buildit() {
  env=$1
  envvsn=$2

  PREFIX=$(cd ..; pwd)/${env}${envvsn}/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  module purge
  module load PrgEnv-${env}/${envvsn}
  module load cmake/3.9.0
  module load python/3.6-anaconda
#  module load openssl/1.1.0c

  build=cupc-build-$vsn
  [ -d $build ] && rm -rf $build
  mkdir -p $build
  cd $build

  cmake ../$dir \
    -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX \
    -DLLVM_TARGETS_TO_BUILD:=host \
    -DCMAKE_BUILD_TYPE:=Release

  export NPROCS=${NPROCS:=32}

  cmake --build . -- -j $NPROCS
  make install
  
  cd ..
  rm -rf $build
}

buildit gnu 4.9
buildit gnu 5.4
buildit gnu 6.3
buildit gnu 7.1
rm -rf $build $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
