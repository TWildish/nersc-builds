#!/bin/bash

cd `dirname $0`
echo "This is obsolete..."
exit 1

openmpi_version=2.1.1
prefix=$(cd ..; pwd)
version=2.22.3
install=${prefix}/${version}

tar=berkeley_upc-${version}.tar.gz
url=http://upc.lbl.gov/download/release/${tar}

set -e
set -x

export TMPDIR=/tmp
builddir=$TMPDIR/build-bupc-${version}
[ -d ${builddir} ] || mkdir ${builddir}
cd ${builddir}

tar -xf $prefix/build/$tar
cd berkeley_upc-${version}
mkdir -p build
cd build

buildAndInstall()
{
  subver=$1

  module load clang/3.2
  CUPC2C_TRANS=$(which clang-upc2c)
  CONF=" CUPC2C_TRANS=$CUPC2C_TRANS --with-multiconf=+dbg_cupc2c,+opt_cupc2c --enable-udp --enable-smp --with-sptr-packed-bits=19,11,34"
  CONF_NOPSHM="$CONF --disable-pshm --disable-aligned-segments"
  CONF_POSIX="--enable-pshm $CONF"
  CONF_SYSV="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-sysv"
  CONF_FILE="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-file"

  module list
  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-smp "
  ../configure CC=$(which gcc) CXX=$(which g++) --prefix=${prefix}/${version}-${subver}-smp $CONF_POSIX
  export NPROCS=${NPROCS:=32}
  make -j $NPROCS
  make install

  # load MPI / inifinband
  module load OFED/2.1-1.0.0-Mellanox
  module load openmpi/${openmpi_version}

  module list
  which mpirun

  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-nopshm "
  IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc --prefix=${prefix}/${version}-${subver}-nopshm $CONF_NOPSHM
  make -j $NPROCS
  make install

  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-posix "
  IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc --prefix=${prefix}/${version}-${subver}-posix $CONF_POSIX
  make -j $NPROCS || make 
  make install

  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-sysv "
  IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc --prefix=${prefix}/${version}-${subver}-sysv $CONF_SYSV
  make -j $NPROCS || make 
  make install

#  make distclean 2>/dev/null || /bin/true
#  echo "Building ${version}-${subver}-file "
#  IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc --prefix=${prefix}/${version}-${subver}-file $CONF_FILE
#  make -j $NPROCS || make 
#  make install

  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-mpi "
  IBVHOME=$OFED_DIR ../configure CC=mpicc CXX=mpic++              MPI_CC=mpicc --prefix=${prefix}/${version}-${subver}-mpi $CONF_SYSV
  make -j $NPROCS || make 
  make install
}

module purge
module load PrgEnv-gnu/4.8
buildAndInstall gnu4.8

if [ 1 == 0 ]
then
module purge
module load PrgEnv-gnu/4.6
buildAndInstall gnu4.6

module purge
module load PrgEnv-gnu/4.7
buildAndInstall gnu4.7
fi

cd ..
rm -rf berkeley_upc-${version} ${builddir}
cd $prefix/build

fix_perms -g jgitools $prefix
echo "Done"
