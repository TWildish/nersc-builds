#!/bin/bash

cd `dirname $0`
set -ex

openmpi_version=2.1.1
version=2.26.0
prefix=$(cd ..; pwd)/

tar=berkeley_upc-${version}.tar.gz
url=http://upc.lbl.gov/download/release/${tar}
dir=berkeley_upc-${version}
[ -f $tar ] || wget $url

export NPROCS=${NPROCS:=32}

buildAndInstall()
{
  pe=$1
  pever=$2

  slurmdir=$(which srun)
  slurmdir=${slurmdir%/*/*}
  pmi_conf="--with-pmi=$slurmdir"
  cupc_install=$CUPC_DIR
  CONF=" CUPC2C_TRANS=${cupc_install}/bin/clang-upc2c --with-multiconf=+dbg_cupc2c,+opt_cupc2c --disable-udp --enable-smp --with-sptr-packed-bits=19,11,34"
  CONF_NOPSHM="$CONF --disable-pshm --disable-aligned-segments"
  CONF_POSIX="--enable-pshm $CONF"
  CONF_SYSV="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-sysv"
  CONF_FILE="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-file"

  [ -d $dir ] && rm -rf $dir
  tar xf $tar
  cd $dir
  
  mkdir -p build
  cd build
  module rm openmpi OFED
  flavor=smp
  p=${prefix}/${pe}${pever}/${version}/${pever}-${flavor}
  mkdir -p $NERSC_BUILD_MODULE_INSTALL_ROOT/jgi/bupc/
  [ -d $p ] && rm -rf $p
  
  module list
  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-${flavor} "
  ../configure CC=$(which gcc) CXX=$(which g++) $pmi_conf --disable-ibv --prefix=${p} $CONF_SYSV
  make -j $NPROCS || make 
  make install
  cat ../../module-template | sed \
    -e "s%_VERSION_%$version%g" \
    -e "s%_BASE_%$p%g" \
    -e "s%_NERSC_BUILD_MODULE_PREREQ_%$prerequisites%g" \
    -e "s%_NERSC_BUILD_MODULE_INSTALL_ROOT_%$NERSC_BUILD_MODULE_INSTALL_ROOT%g" \
  | tee $NERSC_BUILD_MODULE_INSTALL_ROOT/jgi/bupc/${version}-${flavor}

  cd ..
  rm -rf build

  # load MPI / noibv
  mkdir -p build
  cd build
  module rm OFED
  module load openmpi/${openmpi_version}
  flavor=sysv
  p=${prefix}/${pe}${pever}/${version}/${pever}-${flavor}
  [ -d $p ] && rm -rf $p

  module list
  make distclean 2>/dev/null || /bin/true
  echo "Building ${version}-${subver}-${flavor} "
  ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc $pmi_conf --prefix=${p} --disable-ibv $CONF_SYSV
  make -j $NPROCS || make 
  make install
  cat ../../module-template | sed \
    -e "s%_VERSION_%$version%g" \
    -e "s%_BASE_%$p%g" \
    -e "s%_NERSC_BUILD_MODULE_PREREQ_%$prerequisites%g" \
    -e "s%_NERSC_BUILD_MODULE_INSTALL_ROOT_%$NERSC_BUILD_MODULE_INSTALL_ROOT%g" \
  | tee $NERSC_BUILD_MODULE_INSTALL_ROOT/jgi/bupc/${version}-${flavor}

  cd ..
  rm -rf build

  cd ..
  rm -rf $dir
}

prerequisites="cupc/3.9.1-0 perl/5.24.0"
module purge
module load PrgEnv-gnu/7.1
module load $prerequisites
buildAndInstall gnu 7.1

if [ 1 == 0 ]
then
  module purge
  module load PrgEnv-gnu/4.8
  buildAndInstall gnu 4.8

  module load PrgEnv-gnu/5.4
  buildAndInstall gnu 5.4

  module purge
  module load PrgEnv-gnu/6.3
  buildAndInstall gnu 6.3
fi

cd ..
rm -rf $dir
fix_perms -g jgitools $prefix/build
