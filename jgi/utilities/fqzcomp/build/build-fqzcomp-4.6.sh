#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/4.9

VER=4.6
file=fqzcomp-$VER.tar.gz
url=https://sourceforge.net/projects/fqzcomp/files/$file
dir=fqzcomp-$VER
PREFIX=$(cd ..; pwd)/$VER

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

tar xf $file
cd $dir

export NPROCS=${NPROCS:=32}

export CFLAGS="$GCC_RECORD_SWITCHES"
export LDFLAGS="$GCC_RECORD_SWITCHES"
make -j $NPROCS
cp fqz_comp $PREFIX
cd ..
rm -r $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
