#!/bin/bash

set -ex
cd `dirname $0`

module purge
prerequisites="PrgEnv-gnu/7.1 python/2.7-anaconda"

vsn=dev
file=jamo-${vsn}.tar.bz2
dir=$vsn

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

tar --directory $PREFIX --strip-components=1 -xf $file

fix_perms -g jgitools $PREFIX
create-module $PREFIX $prerequisites
