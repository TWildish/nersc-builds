#!/bin/bash

set -ex
module purge
module load PrgEnv-gnu/4.9

vsn=1.0.2
file=DIALIGN-TX_$vsn.tar.gz
url=http://dialign-tx.gobics.de/DIALIGN-TX_1.0.2.tar.gz
dir=DIALIGN-TX_$vsn
PREFIX=$(cd ..; pwd)/$vsn

[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir/source
mkdir -p $PREFIX/bin
make -j 16 CPPFLAGS="-O3 -funroll-loops -mfpmath=sse -msse -mmmx"
mv dialign-tx $PREFIX/bin

cd ../..
rm -rf $dir
