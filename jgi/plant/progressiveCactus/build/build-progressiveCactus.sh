#!/bin/bash

set -ex
module purge
module load PrgEnv-gnu/4.9

url=git://github.com/glennhickey/progressiveCactus.git
dir=progressiveCactus

[ -d $dir ] && rm -rf $dir

git clone $url
cd $dir
git submodule update --init

vsn=`git describe --tags`
PREFIX=$(cd ../..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
mv * $PREFIX

cd ..
rm -rf $dir

cd $PREFIX
make -j 8
