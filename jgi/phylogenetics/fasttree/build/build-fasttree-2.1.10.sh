#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1

vsn=2.1.10
file=FastTree-$vsn.c
url=http://www.microbesonline.org/fasttree/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
mkdir -p $PREFIX/bin

gcc --static -O3 -finline-functions -funroll-loops -Wall -o $PREFIX/bin/FastTree $file -lm

fix_perms -g jgi $PREFIX
create-module $PREFIX
