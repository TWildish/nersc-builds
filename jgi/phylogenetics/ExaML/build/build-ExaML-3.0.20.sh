#!/bin/bash

if [ "$NERSC_HOST" == "denovo" ]; then
  echo "Don't know how to build ExaML on Denovo. There are claims that OpenMP is too slow,"
  echo "but that's the only mpicc in town..."
  exit 0
fi

set -ex
module purge
module load PrgEnv-gnu/6.0.4
module use /usr/common/software/modulefiles
module load impi/2018.0

export NPROCS=16

vsn=3.0.20
file=v$vsn.tar.gz
url=https://github.com/stamatak/ExaML/archive/$file
dir=ExaML-$vsn

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar zxf $file

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX/bin

cd $dir/parser
make -f Makefile.SSE3.gcc
cp parse-examl $PREFIX/bin

cd ../examl

make -j $NPROCS -f Makefile.SSE3.gcc
cp examl $PREFIX/bin
make -f Makefile.SSE3.gcc clean

make -j $NPROCS -f Makefile.AVX.gcc
cp examl-AVX $PREFIX/bin
make -f Makefile.AVX.gcc clean

# Don't build OpenMP versions, they run slow as molasses
#make -j $NPROCS -f Makefile.OMP.AVX.gcc
#cp examl-OMP-AVX $PREFIX/bin
#make -f Makefile.OMP.AVX.gcc clean
#
#make -j $NPROCS -f Makefile.OMP.SSE3.gcc
#cp examl-OMP $PREFIX/bin
#make -f Makefile.OMP.SSE3.gcc clean

cd ../..
rm -rf $dir
fix_perms -g usg $PREFIX/..
create-module $PREFIX
