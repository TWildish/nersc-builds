## Common Modules Reloaded include file
## Author: Douglas Jacobsen <dmj@nersc.gov>
##         User Services Group, NERSC, LBL
## Initial Release: 2013-05-31
## Latest Release : 2013-06-01
## 
## Copyright (C) 2013, Regents of the University of California
## All Rights Reserved
##
## This include file is intended to formalize the structure of inter-
## dependencies between related modulefiles.  Herein module variants 
## (polymorphic modules); automatic loading, unloading, and swapping of pre-
## requisite modules; enhanced error checking: prevention of modulefile load
## if an invalid root path is calculated, prevents unloading/swapping of
## dependent modulefiles, and exits with non-zero (1) exit status upon any
## failure; centralizes common and repeated code from the USG modulefile
## standard.
##
## This include file absolutely requires the following to be true:
##    - $name and $version
##    - $name/$version *must* precisely match the modulefile name
##        specification that a user would use
##
## Optional parameters:
##    - $root: base installation location of targetted software
##    - $mod_conflict: list of modules which conflict ($name should be present)
##    - $mod_prereq_autoload:  list of modules to be auto-loaded
##    - $mod_prereq:  list of modules to enforce "prereq" on
##    - $defined_variations:  list of variants implemented by this modulefile
##    - $mod_variations:  list of variants to-which this modulefile is a client
##    - $mod_fail: string describing why this module can't load
##
## USG-Required parameters (for SW DB):
##    - $fullname:  name of software package
##    - $externalurl:  non-NERSC url for software
##    - $nerscurl:  NERSC url for software
##    - $maincategory:  principal category for software
##    - $subcategory:   subcategory for software
##    - $description:   one-liner description
##
## Environment Variable Sensitivity:
##    - LOADEDMODULES is used to determine which modulefiles are loaded
##    - USG_MODULES_PREREQ is used to track depedencies between modules
##    - (variant)_VER stores the present value for a module variation
##    - (variant)_USERMODULES tracks the variant-depdendent modules
##    - NERSC_PRGENV is current version of special PrgEnv variant
##    - NERSC_PRGENV_USERMODULES tracks PrgEnv-dependent modules
##

## save module-info data that is needed into tcl variables
## this is done because module-info data is overwritten with each successive
## modulefile interpretation; whereas the TCL variables are properly preserved
## on the stack

## mostly stolen from http://wiki.tcl.tk/3757
proc randomString {length} {
    set chars "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    set range [expr {[string length $chars]-1}]

    set txt ""
    for {set i 0} {$i < $length} {incr i} {
        set pos [expr {int(rand()*$range)}]
        append txt [string range $chars $pos $pos]
    }
    return $txt
}

proc lowercase {str} {
    return [string tolower $str]
}

proc import_moduledata {parent child vars} {
    set setlist {}
    foreach var $vars {
        if [info exists $parent\::$var] {
            variable $child\::$var [subst $$parent\::$var]
            unset $parent\::$var
            lappend setlist $var $parent\::$var
        } elseif [info exists ::$var] {
            set t ::$var
            variable $child\::$var [subst $$t]
            unset $t
            lappend setlist $var $t
        }
    }
    return $setlist
}

proc module_action {action target {target2 ""}} {
    set ::temp_module $target
    set ::temp_module2 $target2
    set ::temp_module_action $action
    namespace eval ::[randomString 6] {
        if {$::temp_module2 == ""} {
            module $::temp_module_action $::temp_module
        } else {
            module $::temp_module_action $::temp_module $::temp_module2
        }
    }
    if { ! [info exists ::env(USG_MODULE_UNCLEAN_EXIT) ] } {
        return 0
    }
    return 1
}

proc module_get_chos {} {
    set chos 0
    catch {
        set choslink [readlink /proc/chos/link]
        regexp ".*\/(\[A-Za-z0-9\]+)$" $choslink all_match chos
    }
    return $chos
}

proc ModulesDisplay { } {
    set header "====== MODULES RELOADED ======"
    set messages {}
    if { [info exists ::mod_prereq_autoload] && [llength $::mod_prereq_autoload] > 0 } {
        lappend messages "Autoload Modules    : $::mod_prereq_autoload"
    }
    if { [info exists ::mod_prereq] && [llength $::mod_prereq] > 0 } {
        lappend messages "Prerequisite Modules: $::mod_prereq"
    }
    if { [info exists ::defined_variations] && [llength $::defined_variations] > 0} {
        lappend messages "Defined variations  : $::defined_variations"
    }
    if { [info exists ::mod_variations] && [llength $::mod_variations] > 0} {
        lappend messages "Requires            : $::mod_variations"
    }
    if { [info exists ::mod_bootstrap] && [llength $::mod_bootstrap] > 0} {
        lappend messages "Potential bootstrap modules: $::mod_bootstrap"
    }
    if {[llength $messages] > 0} {
        puts stderr $header
        foreach lmessage $messages {
            puts stderr $lmessage
        }
    }
}

## Required for "module help ..."
proc ModulesHelp { } {
	if {[info exists ::description]} {
		puts stderr "Description - $::description"
	}
	if {[info exists ::nerscurl]} {
		puts stderr "NERSC Docs  - $::nerscurl"
	}
	if {[info exists ::externalurl]} {
		puts stderr "Other Docs  - $::externalurl"
	}
}

## Required for "module display ..." and SWDB
if {[info exists description]} {
	module-whatis "$description"
}


set ::parentns [namespace current]
if {$::parentns == "::"} {
    set ::parentns ""
}
namespace eval ::$parentns {
}

set nsname [randomString 6]
namespace eval ::$nsname {
    ## VARIABLE DECLARATIONS
    variable nsname [namespace current]
    variable name "INVALID"
    variable version "INVALID"
    variable mod_prereq {}
    variable mod_prereq_autoload {}
    variable mod_conflict {}
    variable defined_variations {}
    variable mod_variations {}
    variable mod_fail -1
    variable mod_fail_message ""
    variable conflict_exemption_list [list boost]
    set common_variables {name version mod_prereq mod_prereq_autoload mod_conflict mod_variations defined_variations root mod_fail mod_fail_message}
    set varlist [import_moduledata $::parentns $nsname $common_variables]

    ## internal lists for variant modulefiles
    variable variant_list $defined_variations
    foreach variant $mod_variations {
        lappend variant_list $variant
    }
    lappend variant_list PrgEnv
    set variant_list [lsort -unique $variant_list]
    foreach variant $variant_list {
        set variant [string trim $variant]
	    set env_variant [string toupper $variant]
	    regsub -all {[-,]} $env_variant "_" env_variant
	    variable $variant\_env_var $env_variant\_VER
	    variable $variant\_userdep_env_var $env_variant\_USERMODULES
        variable $variant ""
	}
    foreach variant $defined_variations {
        variable $variant\_selection
        lappend variant_list $variant\_selection
    }
    # if the modulefile defines theses variants, get it
    lappend varlist [import_moduledata $::parentns $nsname $variant_list]

    ## setup non-overridable defaults for PrgEnv
    variable CrayPrgEnv_env_var PE_ENV
    variable CrayPrgEnv_value_hook lowercase
    variable PrgEnv_env_var NERSC_PRGENV
    variable PrgEnv_userdep_env_var NERSC_PRGENV_USERMODULES

    ## read the state of this modulecmd instance
    variable switch0 [module-info mode switch]
    variable switch1 [module-info mode switch1]
    variable switch2 [module-info mode switch2]
    variable switch3 [module-info mode switch3]
    variable mLoad [module-info mode load]
    variable mRemove [module-info mode remove]
    variable specified [module-info specified]
    variable modulefile $::ModulesCurrentModulefile
    variable isRemove [expr { $mRemove && !$switch0 }]
    variable isLoad [expr { ($mLoad || $switch2) && !$switch1 } ]
    variable userReq 1
    variable debug 0

    ## internal lists for maintaining state when dealing with autoloads and
    ## dependency tracking
    variable auto_load_list {}
    variable requisite_modules {}
    variable loaded_modules
    variable variant_error {}

    ## module_log: abstracted logging for the include file
    proc module_log { log_message } {
        variable name
        variable version
        set usgsbin /global/common/shared/usg/sbin
        if [ expr [ file exists $usgsbin/logmod ] ] {
            catch { exec $usgsbin/logmod $name $version }
        }
    }

    ## module_exit: abstracted exit for the include file; sets an environment
    ## variable for modulecmd to communicate the failure to any parent 
    ## instances
    proc module_exit { exit_status log_message } {
        variable specified
        if {$exit_status != 0} {
            set ::env(USG_MODULE_UNCLEAN_EXIT) $specified
            #module_log "exit $exit_status,$log_message"
        }
        exit $exit_status
    }

    ## set_userreq_prereq: determine if this module action should be attributed
    ## to the user.  This can be because the userReq is true, or because the
    ## user interacted with it in the past.
    proc set_userreq_prereq { } {
        variable name
        variable version
        variable userReq
        variable isLoad

        set l_prereq ""
        catch { set l_prereq $::env(USG_MODULES_PREREQ) }

            set prereq_list [split $l_prereq ":"]
        set thismod "$name/$version"
        set userPrereq 0
        if { $userReq == 1 && $isLoad == 1 } {
            set userPrereq 1
        }

        set found [lsearch $prereq_list "USERDEP%$thismod"]
        if { $found >= 0 } {
            set userPrereq 1
        }
        if { $userPrereq == 1 } {
            prepend-path USG_MODULES_PREREQ "USERDEP%$thismod"
        }
    }

    # getVariantValue (variant):
    # determines best value of the variation based for the current module file
    # evaluation context.  If the parent modules are being swapped then it may
    # be that a different context needs to be considered than the obvious
    # values
    proc getVariantValue { variant } {
        variable $variant\_selection
        variable $variant\_env_var
        variable debug
        variable mRemove

        if { $debug } {
            catch { puts stderr "getVariantValue: $variant" }
        }

        if [info exists $variant\_selection] {
            set localvariable_value [subst $$variant\_selection]
        }
        if [info exists $variant\_env_var] {
            set env_var [subst $$variant\_env_var]
            set env_var_old $env_var\_OLD
            set env_var_new $env_var\_NEW
            catch { set oldvariable_value $::env($env_var_old) }
            catch { set currvariable_value $::env($env_var) }
            catch { set newvariable_value $::env($env_var_new) }
        }
        if { $debug } {
            catch { puts stderr "$variant local: $localvariable_value"}
            catch { puts stderr "$variant old: $oldvariable_value" }
            catch { puts stderr "$variant curr: $currvariable_value" }
            catch { puts stderr "$variant new: $newvariable_value" }
        }
        if { $mRemove && [info exists oldvariable_value] } {
            if { $debug } { puts stderr "return old" }
            return $oldvariable_value
        }
        if { [info exists localvariable_value] } {
            if { $debug } { puts stderr "return local" }
            return $localvariable_value
        }
        if { [info exists newvariable_value] } {
            if { $debug } { puts stderr "return new" }
            return $newvariable_value
        }
        if { [info exists currvariable_value] } {
            if { $debug } { puts stderr "return curr" }
            return $currvariable_value
        }
        return 0
    }

    ## auto_load
    ## all external modulesfiles are manipulated by this function
    ## if this modulefile is being ->
    ##      being loaded: load the auto-load list
    ##      swapped in  : swap the auto-load list (unless another loaded module relies on a particular version of the target auto-load module)
    ##      removed     : remove the auto-load list (unless another loaded module relies on the target auto-load module)
    proc auto_load { autoload_list } {
        variable isLoad
        variable isRemove
        variable debug
        variable auto_load_list
        variable loaded_modules
        variable name
        variable version
        variable nsname
        variable conflict_exemption_list

	    set load_list {}
	    set remove_list {}

	    if { $isLoad==0 && $isRemove==0 } {
            return
        }
		foreach l_module $autoload_list {
			set baseModule [string trim $l_module]
			regsub {\/.*} $baseModule {} baseModule

			if {$debug} {
				puts stderr "$name/$version; auto_load: $l_module $baseModule $auto_load_list"
			}

#set alreadyProcessed [lsearch $auto_load_list $baseModule*]
			set alreadyProcessed [lsearch $auto_load_list $l_module]
            if {$alreadyProcessed < 0} {
                set loaded [is-loaded $l_module]
                set baseLoaded [is-loaded $baseModule]
                set conflict_exemption [lsearch $conflict_exemption_list $baseModule]
                set doLoad 0
                set doSwap $loaded
                if {$loaded != 1 && $baseLoaded != 1} {
                    set doLoad 1
                }
                if {$conflict_exemption >= 0 && $loaded != 1} {
                    set doLoad 1
                }
                if {$conflict_exemption < 0 && $baseLoaded == 1} {
                    set doSwap 1
                }
                if {$debug} {
                    puts stderr "auto_load: About to process $baseModule (for $l_module); $loaded, $baseLoaded, doLoad: $doLoad, doSwap: $doSwap"
                }

                if {$isLoad == 1 && $doLoad == 1} {
                    lappend load_list $l_module
                } elseif {$isLoad == 1 && $doSwap == 1} {
                    ## desired swap behavior:
                    ##    ideally swap exact version for exact version
                    ##    if tgt_module is the base module, then swap the first version
                    ##       this is to ensure that a default isn't substituted for a specific version
                    set curr_module $baseModule
                    set tgt_module $l_module
                    set searchPattern "^$baseModule\(\/.*\)?\$"
                    set loaded_module [lsearch -inline -regexp -all $loaded_modules $searchPattern]
                    if { $l_module == $baseModule } {
                        # a generic version was requested, so just reload the
                        # most recent version loaded
                        if {[llength $loaded_module] > 0} {
                            set curr_module [lindex $loaded_module end]
                            set tgt_module [lindex $loaded_module end]
                        }
                    } else {
                        # a specific module version was requested; if that
                        # version is already loaded, then make sure an exact
                        # swap is performed
                        if {[lsearch $loaded_module $l_module] >= 0} {
                            set curr_module $l_module
                            set tgt_module $l_module
                        }
                    }
                    if { [ check_prerequisite_safety $l_module $baseModule 1 0 ] } {
                        if {$debug} {
                            puts stderr "auto_load: Swapping module: $curr_module -> $tgt_module ($loaded_modules)"
                        }
                        if {[module_action swap $curr_module $tgt_module] == 0} {
                            lappend auto_load_list $l_module
                        }
                    }
                } elseif {$isRemove == 1 && $loaded == 1} { 
                    if { [ check_prerequisite_safety $l_module $baseModule 0 1 ] } {
                        lappend remove_list $l_module
                    }
                }
            }
		}
	    foreach l_module $remove_list {
		    if {$debug} {
			    puts stderr "auto_load: Removing module: $l_module"
		    }
            if {[module_action unload $l_module] == 0} {
                lappend auto_load_list $l_module
            }
	    }
	    foreach l_module $load_list {
		    if {$debug} {
			    puts stderr "auto_load: Loading module: $l_module"
		    }
            if {[module_action load $l_module] == 0} {
                lappend auto_load_list $l_module
            }
	    }
    }

    proc enforce_reqs { prereq_list } {
        variable requisite_modules
        variable auto_load_list
        variable specified

	    set foundMissing 0
        foreach l_module $prereq_list {
            set l_module [string trim $l_module]
            set alreadyRequisite [lsearch $requisite_modules $l_module]
            if {$alreadyRequisite < 0} {
                    lappend requisite_modules $l_module
            }
            set alreadyProcessed [lsearch $auto_load_list $l_module]
            if {$alreadyProcessed < 0} {
                set l_foundMissing [catch { prereq $l_module }]
                if { $l_foundMissing == 1 } {
                    set foundMissing 1
                }
            }
        }
	    if { $foundMissing == 1 } {
		    module_exit 1 "prereq failed"
	    }
    }

## Check USG_MODULES_PREREQ to see if the intended operation should be performed on
## $l_module (the argument).
## Returns: 1 if operation can continue
##          0 if operation should be interrupted
##
## if swap, ensure the any modules (other than present) do not require a different
## version of $l_module
##
## if remove, ensure that no modules (other than present) require any version of 
## $l_module
proc check_prerequisite_safety { l_module l_module_base swap_flag remove_flag } {
    variable debug
    variable name
    variable version

	    set prereq_safety ""
	    catch { set prereq_safety $::env(USG_MODULES_PREREQ) }
	    set prereq_safety [split $prereq_safety ":"]
	    set ok 1
	    if { $l_module == $l_module_base && $swap_flag } {
		    return 0
	    }
	    foreach req_group $prereq_safety {
		    set data [split $req_group "%"]
		    set superior [lindex $data 0]
        if { $superior != "$name/$version" } {
            set inferior_list [split [lindex $data 1] ";"]
            set inferior_list [lsearch -inline -all -regexp $inferior_list "^$l_module_base\(\/.*\)?\$"]
            foreach inferior $inferior_list {
                set is_base [expr { $inferior == $l_module_base }]
                set is_exact [expr { $inferior == $l_module }]

                if { $swap_flag } {
                    if { [expr { !$is_exact && !$is_base } ] } {
                        set ok 0
                    }
                }
                if { $remove_flag } {
                    if {$debug} {
                        puts stderr "preventing unload of $l_module: $inferior"
                    }
                    set ok 0
                }
            }
        }
	    }
	    return $ok
}

proc check_user_dependencies { variant } {
    variable $variant\_userdep_env_var
    variable $variant\_userdep
    set userDeps_loaded ""
    set userdep_var_name $variant\_userdep_env_var
    if {[info exists $userdep_var_name]} {
        set env_var [subst $$userdep_var_name]
        catch { set userDeps_loaded $::env($env_var) }
    }
    set userdep_ptr $variant\_userdep
    if { [info exists $userdep_ptr] } { set userDeps_loaded [subst $$userdep_ptr] }
    set $userdep_ptr $userDeps_loaded

    set userDep_list [split $userDeps_loaded ":"]
#    set userDep_list [lsearch -inline -all -not $userDep_list *PREMARKER* ]

    return $userDep_list
}

proc process_user_dependencies { variant } {
    variable isLoad
    variable nsname
    variable mod_prereq_autoload
    set dep_list [check_user_dependencies $variant]
    if {$isLoad} {
        foreach l_module $dep_list {
            set found [lsearch $mod_prereq_autoload $l_module]
            if { $found < 0 } {
                lappend mod_prereq_autoload $l_module
            }
        }
    }
}

## Check to see if a change is coming in the default
proc check_default_change { } {
    variable name
    variable version
    variable specified
    variable modulefile
    ## was the module request a 'default' module
    if { $specified == $name } {
        set path_components [file split $modulefile ]
        set path_core [join [lrange $path_components 1 end-1] "/"]
        set newversion_file "/$path_core/.newversion"
        set lversion $version
        set ldate ""
        set custom_message ""
        if {[file exists $newversion_file]} {
            set fd [open $newversion_file]
            set bytes [gets $fd line]
            if { $bytes > 0 } {
                set lversion $line
            }
            set bytes [gets $fd line]
            if { $bytes > 0 } {
                set ldate $line
            }
            set bytes [gets $fd line]
            if { $bytes > 0 } {
                set custom_message $line
            }
            close $fd
        }
        if {$lversion != $version} {
            puts stderr "WARNING: The default version of $name will be changing from $version to $lversion on $ldate.  Please try $name/$lversion. $custom_message Please contact consult@nersc.gov with any questions."
        }
    }
}


    ## MAIN PROGRAM
    catch { set userReq [expr { $::env(NERSC_PARENT_MOD) == $name } ] }
    if {$userReq} { set ::env(NERSC_PARENT_MOD) $name }

    catch { set debug $::env(MODULE_DEBUG) }
    if { $debug } {
	    puts stderr "userReq $userReq; remove: $isRemove; load: $isLoad; Modes: load $mLoad; remove $mRemove; switch1 $switch1; switch2 $switch2; switch3 $switch3; $name/$version; $nsname"
	    puts stderr "$ModulesCurrentModulefile; [module-info flags]; [module-info specified]"
    }

    set_userreq_prereq

    ## get list of loaded modules
    set loaded_modules ""
    catch { set loaded_modules $env(LOADEDMODULES) }
    set loaded_modules [split $loaded_modules ":"]


    ## check the mod_conflict list and dump out if necessary
	set foundConflict 0
	foreach l_module $mod_conflict {
        set l_module [string trim $l_module]
		set l_foundConflict [catch { conflict $l_module }]
		if { $l_foundConflict == 1 } {
			set foundConflict 1
		}
	}
	if { $foundConflict == 1 } {
		module_exit 1 "conflict"
	}

	foreach variant $defined_variations {
		set variantval $variant\_selection
		if { [info exists $variantval] } {
			set envvar $variant\_env_var
            if { [info exists $envvar] } {
                set var [subst $$envvar]
                set val [subst $$variantval]
                set var_old $var\_OLD
                set var_new $var\_NEW
                catch {
                    set oldval $::env($var)
                    set ::env($var_old) $oldval
                }
                set ::env($var_new) $val
			    setenv $var $val
            } else {
                puts stderr "$variant variant is misconfigured."
            }
		}
        process_user_dependencies $variant
	}
    ## iterate through each module variant dimension and setup variant values for
    ## this evaluation of the modulefile
    foreach variant $mod_variations {
        set l_value [getVariantValue $variant]
        if [info exists $variant\_value_hook] {
            set l_value [[subst $$variant\_value_hook] $l_value]
        }
        if {$l_value == 0} {
            puts stderr "ERROR: Couldn't determine a value for variant $variant!"
            lappend variant_error $variant
        } else {
            set $variant $l_value

            if {[lsearch $defined_variations $variant] < 0} {
                set userdep_var_name $variant\_userdep_env_var
                if {[info exists $userdep_var_name]} {
                    set env_var [subst $$userdep_var_name]
                    prepend-path $env_var $name/$version
                }
            }
        }
    }
	if { [llength $variant_error] > 0} {
		puts stderr "ERROR: Can't load $name/$version"
		module_exit 1 "Can't load $name/$version"
	}

    ## setup root path and verify existance
    if [info exists root] {
	    if {$debug} {
		    puts stderr "got root $root"
	    }
	    set rootErr [catch { set root [subst $root] }]
	    if { $rootErr != 0 } {
		    puts stderr "Couldn't determine software location for $name: $root"
		    module_exit 1 "no resolve root: $root"
	    }
	    if { ![file exists $root] } {
		    puts stderr "$name/$version does not appear to have a valid directory: $root doesn't exist. Exiting without changing modules."
		    if { $switch0 } {
			    puts stderr "Please contact consult@nersc.gov to request a compatible version, or unload $name/$version and try again."
		    } else {
			    puts stderr "Please contact consult@nersc.gov to request a compatible version."
		    }
		    module_exit 1 "no existing root: $root"
	    }
    }

    ## check for any auto-loads
    auto_load $mod_prereq_autoload

    ## check for prereqs
    enforce_reqs $mod_prereq

    ## log the event if loading
    if {$isLoad} {
        module_log "load"
    }

    ## set prereq env var
    if {[llength $requisite_modules] > 0} {
	    set requisite_modules_str [join $requisite_modules ";"]
	    set requisite_modules_str "$name/$version%$requisite_modules_str"
        prepend-path USG_MODULES_PREREQ $requisite_modules_str
    }

    check_default_change

    set idx 0
    set limit [llength $varlist]
    while {$idx<$limit} {
        set src [lindex $varlist $idx]
        incr idx
        set tgt [lindex $varlist $idx]
        incr idx
        set $tgt [subst $$src]
    }
}

