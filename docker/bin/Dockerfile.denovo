FROM opensuse:42.3

ENV MODULEPATH=/nersc-builds/denovo/modules/installed/usg:/nersc-builds/denovo/modules/installed/jgi \
    NPROCS=8 \
    NERSC_HOST=denovo \
    BASH_ENV=/etc/profile.d/module.sh \
    PS1='\u@\h: \W> '
SHELL ["/bin/bash", "-l", "-c"]

RUN zypper --non-interactive install --no-recommends \
            wget git python tcl autoconf make tar which vim \
            glibc-devel-static glibc-devel libglib-2_0-0 glib2-devel \
            libX11-devel libXpm-devel fontconfig && \
    \
    useradd --user-group gpinterp && \
    groupadd usg && \
    groupadd jgi && \
    groupadd jgitools && \
    usermod --groups usg,jgi gpinterp && \
    \
    cd / && \
    git clone https://bitbucket.org/TWildish/nersc-builds.git && \
    cp /nersc-builds/docker/bin/fix_perms /usr/local/bin && \
    chmod 755 /usr/local/bin/fix_perms && \
    cd /nersc-builds/denovo/bin && source ./install-modules.sh && \
    \
    cp /nersc-builds/denovo/bin/runnit.sh /bin/runnit.sh

WORKDIR /nersc-builds/docker/bin
RUN zypper --non-interactive install --no-recommends gcc && \
    /bin/runnit.sh /nersc-builds/docker/bin/build-modules-3.2.10.sh && \
    cp /usr/local/bin/Modules/3.2.10/init/bash /etc/profile.d/module.sh && \
    source /etc/profile.d/module.sh && \
    /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-4.6.3.sh&& \
    zypper --non-interactive remove gcc && \
    chown -Rf gpinterp.gpinterp /nersc-builds
USER gpinterp

RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-4.8.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-4.9.2.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-5.4.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-6.2.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-6.3.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/gcc/build/build-gcc-7.1.0.sh

RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/zlib/build/build-zlib-1.2.11.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/libpng/build/build-libpng-1.6.28.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/libfreetype/build/build-libfreetype-2.7.1.sh
# RUN ln -s /usr/lib64/libglib-2.0.so.0 /usr/lib64/libglib-2.0.so
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/gnustuff/build/all-build.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/cmake/build/build-cmake-3.4.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/cmake/build/build-cmake-3.7.2.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/cmake/build/build-cmake-3.9.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/pcre/build/build-pcre-8.40.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/xz/build/build-xz-5.2.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/bzip2/build/build-bzip2-1.0.6.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/bzip2/build/build-pbzip2-1.1.13.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/binutils/build/build-binutils-2.27.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/binutils/build/build-binutils-2.28.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/python/build/build-python-2.7-anaconda-4.4.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/python/build/build-python-3.5-anaconda-4.2.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/python/build/build-python-3.6-anaconda-4.4.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/readline/build/build-readline-6.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/readline/build/build-readline-7.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/texinfo/build/build-texinfo-6.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/texinfo/build/build-texinfo-6.4.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/hdf5/build/build-hdf5-1.8.15-threadsafe.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/hpc/openmpi/build/build-openmpi-1.6.5.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/hpc/openmpi/build/build-openmpi-1.8.7.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/hpc/openmpi/build/build-openmpi-2.1.1.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/math/gsl/build/build-gsl-2.3.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/gdb/build/build-gdb-7.12.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/libjpeg/build/build-libjpeg-6b.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/libgd/build/build-libgd-2.2.4.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/boost/build/build-boost-1.59.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/boost/build/build-boost-1.61.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utility_libraries/boost/build/build-boost-1.63.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.16.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.18.2.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.24.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/openssl/build/build-openssl-1.1.0c.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/curl/build/build-curl-7.52.1.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/git/build/build-git-2.11.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/postgresql/build/build-postgresql-9.6.2.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/mysql/build/build-mysql-5.5.25a.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/mysql/build/build-mysql-5.5.56.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/mysql/build/build-mysql-5.6.14.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/mysql/build/build-mysql-5.6.36.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/utilities/mysql/build/build-mysql-5.7.19.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.16.0.modules.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.18.2.modules.sh
RUN /bin/runnit.sh /nersc-builds/denovo/usg/languages/perl/build/build-perl-5.24.0.modules.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/assemblers/NOVOPlasty/build/build-NOVOPlasty-2.4.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/assemblers/NOVOPlasty/build/build-NOVOPlasty-2.5.6.sh
# RUN /bin/runnit.sh /nersc-builds/denovo/jgi/utilities/emacs/build/build-emacs-25.1.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/utilities/fqzcomp/build/build-fqzcomp-4.6.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/aligners/blast+/build/build-blast+-2.6.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/file_formats/samtools/build/build-samtools-1.4.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/utilities/quast/build/build-quast-4.5.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/assemblers/spades/build/build-spades-3.10.1.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/math/gnuplot/build/build-gnuplot-4.6.2.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/math/gnuplot/build/build-gnuplot-5.0.5.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/assemblers/meraculous/build/build-meraculous-2.2.4.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/aligners/muscle/build/build-muscle-3.8.1551.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/aligners/muscle/build/build-muscle-3.8.31.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/frameworks/pasa/build/build-pasa-2.1.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/frameworks/EMBOSS/build/build-EMBOSS-6.6.0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/phylogenetics/Gblocks/build/build-Gblocks-0.91b.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/languages/cupc/build/build-cupc-3.9.1-0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/languages/cupc/build/build-cupc-3.8.1-0.sh
RUN /bin/runnit.sh /nersc-builds/denovo/jgi/assemblers/trinity/build/build-trinity-2.3.2.sh

#
# docker build -t tonywildish/denovo:latest -f Dockerfile.denovo .
