#!/bin/bash

set -ex
vsn=3.2.10
file=modules-$vsn.tar.gz
dir=modules-$vsn
url=https://downloads.sourceforge.net/project/modules/Modules/$dir/$file
if [ $EIUD ]; then
  echo "Not building module command since you're not root"
  exit 0
fi
PREFIX=/usr/local/bin
NPROCS=${NPROCS:=16}

tclvsn=8.6.7
tclfile=tcl${tclvsn}-src.tar.gz
tclurl=https://prdownloads.sourceforge.net/tcl/$tclfile
tcldir=tcl${tclvsn}
[ -f $tclfile ] || wget -O $tclfile $tclurl
[ -d $tcldir ] && rm -rf $tcldir
tar xf $tclfile
cd $tcldir/unix
./configure
make -j $NPROCS
make install
cd ../..
rm -rf $tcldir

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
export CPPFLAGS=-DUSE_INTERP_ERRORLINE
./configure --prefix=$PREFIX
make -j $NPROCS
echo "Don't 'make test' because that needs DejaGnU, which we aren't bothering with"
make install
cd ..
rm -rf $dir
 
echo "$PREFIX/Modules/3.2.10/bin/modulecmd sh \$@" | tee $PREFIX/module
chmod +x $PREFIX/module
cp $PREFIX/Modules/3.2.10/init/bash /etc/profile.d/module.sh
