#!/bin/bash

cd `dirname $0`/../..
gcc_dir=`pwd`/denovo/usg/languages/gcc
prgenv_mod_dir=denovo/modules/installed/usg/PrgEnv-gnu

cd $prgenv_mod_dir
for f in `ls`
do
  if [ `ls $gcc_dir | egrep "${f}\." | wc -l` -gt 0 ]; then
    echo Have $f
  else
    echo Do not have $f
    rm -rf $f
  fi
done
