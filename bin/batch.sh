#!/bin/bash
#SBATCH --mail-user wildish@lbl.gov
#SBATCH --mail-type all
#SBATCH --time 1-00:00:00
#SBATCH --output /global/homes/w/wildish/slurm.nersc_builds%j.out
#SBATCH --error  /global/homes/w/wildish/slurm.nersc_builds%j.err

set -ex
cd /scratch # Assumes Denovo!
mkdir -p $USER/build-$NERSC_HOST/$SLURM_JOBID
cd $USER/build-$NERSC_HOST/$SLURM_JOBID

git clone https://bitbucket.org/TWildish/nersc-builds.git
cd nersc-builds/bin

set -x
./make-build-everything.sh | tee doit.sh
chmod +x doit.sh
./doit.sh
