#!/bin/bash

cat <<EOF

 Remove all built products manually...

EOF

cd `dirname $0`/../

for dir in `ls -d \
    {usg,jgi}/*/*/gnu* \
    {usg,jgi}/*/*/[0-9]*.*[0-9] \
    usg/languages/python/[0-9]*anaconda 2>/dev/null`
do
  echo $dir
  rm -rf $dir
done
