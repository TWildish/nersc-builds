#!/bin/bash

here=$(cd `dirname $0`; pwd)
source $here/clean-environment.sh
#export PATH=$here:/bin:/usr/bin:/usr/local/bin

task=$1
start_dir=`echo $task | sed -e 's%/[^/]*$%%'`
build_script=`echo $task | awk -F/ '{ print $NF }'`
filelist=../runnit-filelist.txt

set -x
cd $start_dir
if [ -z "$NERSC_BUILD_NO_CLEAN_SOURCES" ]; then
  ls | tee $filelist
fi

echo "Execute $build_script"
./$build_script
status=$?
echo "Exit with status $status"
if [ $status -ne 0 ]; then
  echo "Spitting the dummy on $build_script"
  exit 1
fi
echo "=============================="
echo " "

# Clean up the installation a bit.
if [ -z "$NERSC_BUILD_NO_CLEAN_SOURCES" ]; then
  for f in *
  do
    [ `grep -c $f $filelist` -gt 0 ] || ( echo "Cleaning $f" && rm -rf $f )
  done
  rm $filelist
fi

find .. -type d -name bin -exec strip {}/* \; >/dev/null 2>&1
