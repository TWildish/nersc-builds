#!/bin/bash

set -ex
if [ -z "$NERSC_BUILD_MODULE_INSTALL_ROOT" ]; then
  echo "NERSC_BUILD_MODULE_INSTALL_ROOT not defined, spitting the dummy..."
  exit 1
fi

HERE=`pwd`


if [ -z "$NERSC_BUILD_NO_FORCE_REBUILD" ]; then
  rm -rf   $NERSC_BUILD_MODULE_INSTALL_ROOT/{usg,jgi}
fi

for d in usg jgi
do
  mkdir -p $NERSC_BUILD_MODULE_INSTALL_ROOT/$d
done
