#!/bin/bash
#
# Assert the following:
#
# - the first argument is the installation directory of the build products
#   - that path follows one of two forms:
#     /.../$group/$package/$version
#     or
#     /.../$group/$package/$PrgEnv/$version
#
# - NERSC_BUILD_MODULE_INSTALL_ROOT points to the location where module files will be installed
#   - this environment variable is mandatory
#   - there is a module template, $NERSC_BUILD_MODULE_INSTALL_ROOT/module-template
#   - the target module is $NERSC_BUILD_MODULE_INSTALL_ROOT/$group/$package/$version
#
# - package-specific module definitions are held in a file module-template. These override defaults
# - version-specific module definitions are held in a file module-template.$vsn. These override defaults
#   and override package-specific module definitions too
#

PREFIX=$1
if [ ! -d $PREFIX ]; then
  echo "No installation found: PREFIX=$PREFIX"
  exit 0
fi

prerequisites=$2

vsn=`basename $PREFIX`

if [ -z "$NERSC_BUILD_MODULE_INSTALL_ROOT" ]; then
  echo "NERSC_BUILD_MODULE_INSTALL_ROOT not defined, spitting the dummy..."
  exit 1
fi

template=$NERSC_BUILD_MODULE_INSTALL_ROOT/module-template
[ -f "module-template"      ] && template="$template module-template"
[ -f "module-template.$vsn" ] && template="$template module-template.$vsn"

#
# Take the PREFIX, strip off the version, look for a PrgEnv
base=`dirname $PREFIX`
if [ `echo $base | egrep -c '/gnu[0-9]\.[0-9]$'` == 1 ]; then
  prgenv=1
  base=`dirname $base`
else
  prgenv=""
fi

package=`basename $base`
PACKAGE=`echo $package | tr '[a-z]' '[A-Z]'`

base=`dirname $base`
group=`dirname $base`
group=`basename $group`

if [ "$prgenv" == 1 ]; then
  base="$base/\$name/\$PrgEnv/\$version"
  prgenv="set mod_variations { PrgEnv }"
else
  base="$base/\$name/\$version"
  prgenv=""
fi

mkdir -p $NERSC_BUILD_MODULE_INSTALL_ROOT/$group/$package/
dest=$NERSC_BUILD_MODULE_INSTALL_ROOT/$group/$package/$vsn

echo "Install module file for $package/$vsn using $template as the template"
(
  cat $template | sed \
    -e "s%_VERSION_%$vsn%g" \
    -e "s%_PACKAGE_%$package%g" \
    -e "s%_BASE_%$base%g" \
    -e "s%_PRGENV_%$prgenv%g" \
    -e "s%_NERSC_BUILD_MODULE_PREREQ_%$prerequisites%g" \
    -e "s%_NERSC_BUILD_MODULE_INSTALL_ROOT_%$NERSC_BUILD_MODULE_INSTALL_ROOT%g"

  if [ `cat $template | grep -c ${PACKAGE}_INC` -eq 0 ]; then
    [ -d $PREFIX/include ] && echo "setenv ${PACKAGE}_INC -I\$root/include"
  fi

  if [ `cat $template | grep -c ' PATH '` -eq 0 ]; then
    [ -d $PREFIX/sbin ] && echo "prepend-path PATH \$root/sbin"
    [ -d $PREFIX/bin  ] && echo "prepend-path PATH \$root/bin"
  fi

  if [ `cat $template | grep -c MANPATH` -eq 0 ]; then
    [ -d $PREFIX/share/man ] && echo "prepend-path MANPATH \$root/share/man"
  fi

  if [ `cat $template | grep -c PKG_CONFIG_PATH` -eq 0 ]; then
    [ -d $PREFIX/lib/pkgconfig ] && echo "prepend-path PKG_CONFIG_PATH \$root/lib/pkgconfig"
  fi

  pkg_lib=""
  ld_library_path=""
  if [ -d $PREFIX/lib   ]; then
    ld_library_path="\$root/lib"
    pkg_lib="-L\$root/lib"
  fi
  if [ -d $PREFIX/lib64 ]; then
    [ ! -z $ld_library_path ] && ld_library_path="${ld_library_path}:"
    ld_library_path="${ld_library_path}\$root/lib64"
    pkg_lib="$pkg_lib -L\$root/lib64"
  fi

  if [ `cat $template | grep -c " ${PACKAGE}_LIB "` -eq 0 ]; then
    [ ! -z "$pkg_lib" ] && echo "setenv ${PACKAGE}_LIB \"$pkg_lib\""
  fi

  if [ `cat $template | grep -c " $LD_LIBRARY_PATH "` -eq 0 ]; then
    [ ! -z $ld_library_path ] && echo "prepend-path LD_LIBRARY_PATH $ld_library_path"
  fi

  if [ `cat $template | grep -c " ${PACKAGE}_DIR "` -eq 0 ]; then
    echo "setenv ${PACKAGE}_DIR \$root"
  fi

) | tee $dest
echo "Installed $dest"
