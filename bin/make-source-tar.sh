#!/bin/bash

cd `dirname $0`
here=`pwd`
tarball=$here/dist.tar
cd ../

tar cvf $tarball \
  `ls {usg,jgi}/*/*/build/* | \
   egrep '\.tar.gz$|\.tar.bz2$|\.tar.xz$|\.tar$|\.zip$|Anaconda.*\.sh|*.c$'`

ls -lh $tarball
