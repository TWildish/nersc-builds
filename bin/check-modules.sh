#!/bin/bash

cd `dirname $0`/..

cat <<EOF

# List of module directories missing w.r.t build scripts
EOF

for tool in `ls -d  {usg,jgi}/*/*/build | awk -F/ '{ print $1"/"$3 }'`
do
  ls -ld Modules/$tool >/dev/null
done 2>&1 | sed -e 's%^.*Modules/%Modules/%' -e 's% .*$%%' | tr -d ':'

cat <<EOF

# List of build scripts missing w.r.t. module versions
EOF

for top in usg jgi
do
  for tool in `ls -d  Modules/$top/* | awk -F/ '{ print $NF }' | egrep -v 'PrgEnv-gnu|OFED'`
  do
    for vsn in `ls Modules/$top/$tool/`
    do
      ls $top/*/$tool/build | egrep -i "build-${tool}-${vsn}*.sh" >/dev/null
    done
  done
done 2>&1 | sed -e "s%^[^ ]* %%" -e 's% .*$%%' | tr -d "':"
