#!/bin/bash

cat <<EOF

# Scripts listed in build-order.txt that don't actually exist...
EOF
for f in `cat build-order.txt  | egrep -v '#|^$'`
do
  [ -f ../$f ] || echo $f
done

cat <<EOF

# Existing build scripts that are not enabled in build-order.txt
EOF

cd `dirname $0`/..
for f in `ls {usg,jgi}/*/*/build/build*.sh | \
            egrep -v '/gnustuff/|gcc/build/build-gcc-generic.sh'`
do
  echo $f `grep -c $f bin/build-order.txt`
done | \
 egrep ' 0$' | \
 awk '{ print $1 }'
